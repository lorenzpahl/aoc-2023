// --- Day 1: Trebuchet?! ---

import { open } from "node:fs/promises";
import { resolve } from "node:path";
import { sumOf } from "./util/iterators.js";

const digitsByName = [
	["1", 1],
	["2", 2],
	["3", 3],
	["4", 4],
	["5", 5],
	["6", 6],
	["7", 7],
	["8", 8],
	["9", 9]
];

export async function part1(calibrationDocumentUrl = resolve("resources/day01/input01.txt")) {
	const calibrationLineMatcherConfig = mkCalibrationLineMatcherConfig(digitsByName);
	const calibrationValues = await readCalibrationValues(calibrationDocumentUrl, calibrationLineMatcherConfig);
	return sumOf(calibrationValues);
}

export async function part2(calibrationDocumentUrl = resolve("resources/day01/input01.txt")) {
	const calibrationLineMatcherConfig = mkCalibrationLineMatcherConfig([
		...digitsByName,
		["one", 1],
		["two", 2],
		["three", 3],
		["four", 4],
		["five", 5],
		["six", 6],
		["seven", 7],
		["eight", 8],
		["nine", 9]
	]);

	const calibrationValues = await readCalibrationValues(calibrationDocumentUrl, calibrationLineMatcherConfig);
	return sumOf(calibrationValues);
}

async function readCalibrationValues(calibrationDocumentUrl, calibrationLineMatcherConfig) {
	const calibrationValues = [];
	const calibrationDocumentFileDescriptor = await open(calibrationDocumentUrl);
	for await (let calibrationLine of calibrationDocumentFileDescriptor.readLines()) {
		calibrationValues.push(parseCalibrationValue(calibrationLine, calibrationLineMatcherConfig));
	}

	return calibrationValues;
}

function mkCalibrationLineMatcherConfig(digitsByName) {
	return {
		*digitNameMatches(calibrationLine) {
			for (let [digitName, digit] of digitsByName) {
				const digitNamePattern = new RegExp(digitName, "g");
				for (let digitNameMatch of calibrationLine.matchAll(digitNamePattern)) {
					const startIndex = digitNameMatch.index;
					const endIndex = startIndex + digitName.length;
					yield { digit, startIndex, endIndex };
				}
			}
		}
	};
}

function parseCalibrationValue(calibrationLine, { digitNameMatches }) {
	const nil = {
		digit: Number.NaN,
		startIndex: Number.MAX_SAFE_INTEGER,
		endIndex: Number.MIN_SAFE_INTEGER
	};

	const [{ digit: firstDigit }, { digit: lastDigit }] =
		Array.from(digitNameMatches(calibrationLine))
			.reduce(([prevFirstDigitInfo, prevLastDigitInfo], currDigitInfo) => {
				const { startIndex: prevFirstStartIndex } = prevFirstDigitInfo;
				const { endIndex: prevLastEndIndex } = prevLastDigitInfo;
				const { startIndex: currStartIndex, endIndex: currEndIndex } = currDigitInfo;

				return [
					(currStartIndex < prevFirstStartIndex) ? currDigitInfo : prevFirstDigitInfo,
					(currEndIndex > prevLastEndIndex) ? currDigitInfo : prevLastDigitInfo
				];
			}, [nil, nil]);

	return Number(`${firstDigit}${lastDigit}`);
}

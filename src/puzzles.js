import { select } from "@inquirer/prompts";
import { part1 as day01a, part2 as day01b } from "./day01.js";
import { part1 as day02a, part2 as day02b } from "./day02.js";
import { createEngineSchematic, part1 as day03a, part2 as day03b } from "./day03.js";
import { part1 as day04a, part2 as day04b, readAllScratchcards } from "./day04.js";
import { part1 as day05a, part2 as day05b, readAlmanac } from "./day05.js";
import { part1 as day06a, part2 as day06b } from "./day06.js";
import { part1 as day07a, part2 as day07b } from "./day07.js";
import { part1 as day08a, part2 as day08b } from "./day08.js";
import { run as day09 } from "./day09.js";
import { run as day10 } from "./day10.js";
import { part1 as day11a, part2 as day11b } from "./day11.js";

function* generatePuzzleChoice() {
	for (let i = 1; i <= 25; i++) {
		const enabled = i <= 11;
		yield {
			name: `Day ${i}`,
			value: `day${String(i).padStart(2, "0")}`,
			disabled: !enabled
		};
	}
}

const selectedPuzzle = await select({
	message: "Select a puzzle",
	choices: [...generatePuzzleChoice()]
});

switch (selectedPuzzle) {
	case "day01": {
		const calibrationValueSum = await day01a();
		console.log(`Part I: the sum of all the calibration values is ${calibrationValueSum}.`);

		const calibrationValueSum2 = await day01b();
		console.log(`Part II: the sum of all the calibration values is ${calibrationValueSum2}.`);
		break;
	}
	case "day02": {
		const sumOfPossibleGameIds = await day02a();
		console.log(`Part I: the sum of the possible game IDs is ${sumOfPossibleGameIds}.`);

		const sumOfPower = await day02b();
		console.log(`Part II: the sum of the power is ${sumOfPower}.`);
		break;
	}
	case "day03": {
		const engineSchematic = await createEngineSchematic();
		const sumOfPartNumbers = day03a(engineSchematic);
		console.log(`Part I: the sum of all of the part numbers in the engine schematic is ${sumOfPartNumbers}.`);

		const sumOfGearRatios = day03b(engineSchematic);
		console.log(`Part II: the sum of all of the gear ratios in the engine schematic is ${sumOfGearRatios}.`);
		break;
	}
	case "day04": {
		const scratchcards = await readAllScratchcards();
		const sumOfScratchcardPoints = day04a(scratchcards);
		console.log(`Part I: the scratchcards are worth ${sumOfScratchcardPoints} points in total.`);

		const numberOfScratchcards = day04b(scratchcards);
		console.log(`Part II: you have a total of ${numberOfScratchcards} scratchcards.`);
		break;
	}
	case "day05": {
		const almanac = await readAlmanac();
		const lowestLocationNumber = day05a(almanac);
		console.log(`Part I: the lowest location number is ${lowestLocationNumber}.`);

		const lowestLocationNumber2 = day05b(almanac);
		console.log(`Part II: the lowest location number is ${lowestLocationNumber2}.`);
		break;
	}
	case "day06": {
		const result = await day06a();
		console.log(`Part I: the product of the number of ways you can beat the record is ${result}.`);

		const result2 = await day06b();
		console.log(`Part II: you can beat the record in ${result2} ways.`);
		break;
	}
	case "day07": {
		const totalWinnings = await day07a();
		console.log(`Part I: the total winnings are ${totalWinnings}.`);

		const totalWinnings2 = await day07b();
		console.log(`Part II: using the new joker rule, the total winnings are ${totalWinnings2}.`);
		break;
	}
	case "day08": {
		const stepCount = await day08a();
		console.log(`Part I: ${stepCount} steps are required to reach ZZZ.`);

		const stepCount2 = await day08b();
		console.log(`Part II: it takes ${stepCount2} steps to end up entirely on nodes that end in Z.`);
		break;
	}
	case "day09": {
		const [sumOfExtrapolatedStartValues, sumOfExtrapolatedEndValues] = await day09();
		console.log(`Part I: the sum of the extrapolated values is ${sumOfExtrapolatedEndValues}.`);
		console.log(`Part II: the sum of the extrapolated previous values is ${sumOfExtrapolatedStartValues}.`);
		break;
	}
	case "day10": {
		const { farthestDistance, enclosedTileCount } = await day10();
		console.log(`Part I: it takes ${farthestDistance} steps to get to the point farthest from the starting position.`);
		console.log(`Part II: ${enclosedTileCount} tiles are enclosed by the loop.`);
		break;
	}
	case "day11": {
		const sumOfLengths = await day11a();
		console.log(`Part I: the sum of the lengths is ${sumOfLengths}.`);

		const sumOfLengths2 = await day11b();
		console.log(`Part II: the sum of the lengths is ${sumOfLengths2}.`);
		break;
	}
	default: {
		throw new Error(`Unable to handle puzzle ${selectedPuzzle}!`);
	}
}

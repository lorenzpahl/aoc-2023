// --- Day 10: Pipe Maze ---

import { open } from "node:fs/promises";
import { resolve } from "node:path";
import { comparingNumber, thenComparing } from "./util/comparators.js";
import { distinctBy, maxBy, sumOf, zip, zipWithIndex } from "./util/iterators.js";
import { computeMapEntry } from "./util/maps.js";

export async function run(pipeLocationsUrl = resolve("resources/day10/input01.txt")) {
	const areaMap = await parsePipes(pipeLocationsUrl);
	const [loopPath, distance] = areaMap.locateFarthestTile();
	const enclosedTileCount = numberOfInteriorPoints(loopPath);
	return { enclosedTileCount, farthestDistance: distance };
}

export async function part1(pipeLocationsUrl) {
	const { farthestDistance } = await run(pipeLocationsUrl);
	return farthestDistance;
}

export async function part2(pipeLocationsUrl) {
	const { enclosedTileCount } = await run(pipeLocationsUrl);
	return enclosedTileCount;
}

/**
 * Calculates the number of points enclosed by the loop using Pick's theorem:
 *
 * - https://en.wikipedia.org/wiki/Pick%27s_theorem
 * - https://en.wikipedia.org/wiki/Polygon#Area
 */
function numberOfInteriorPoints(loopPath) {
	function calculateArea() {
		const [first, ...rest] = loopPath;
		const vs =
			Array.from(zip(loopPath, [...rest, first])).map(([lhs, rhs]) => {
				const { column: x1, row: y1 } = lhs.position;
				const { column: x2, row: y2 } = rhs.position;
				return (y1 + y2) * (x1 - x2);
			});

		return Math.abs(sumOf(vs)) / 2;
	}

	const a = calculateArea();
	const b = loopPath.length;
	return -(b / 2) + 1 + a;
}

async function parsePipes(pipeLocationsUrl) {
	let row = 0;
	let startingPosition = undefined;
	const gridOfTiles = [];
	const pipeLocationsFileHandle = await open(pipeLocationsUrl);
	for await (const line of pipeLocationsFileHandle.readLines()) {
		const rowDescriptor =
			Array.from(line).reduce((prevRowDescriptor, tileKind, column) => {
				const tile = Tile.of(tileKind);
				const prevStartingPosition = prevRowDescriptor.startingPosition;
				const prevRowTiles = prevRowDescriptor.rowTiles;
				return {
					rowTiles: [...prevRowTiles, tile],
					startingPosition: tile.isSTile() ? new Position(row, column) : prevStartingPosition
				};
			}, { rowTiles: [], startingPosition: undefined });

		gridOfTiles.push(rowDescriptor.rowTiles);
		if (rowDescriptor.startingPosition) {
			startingPosition = rowDescriptor.startingPosition;
		}

		row++;
	}

	return new AreaMap(gridOfTiles, startingPosition);
}

class AreaMap {

	constructor(gridOfTiles, startingPosition) {
		if (!startingPosition) throw new Error("starting position is mandatory.");

		this.gridOfTiles = gridOfTiles;
		this.startingPosition = startingPosition;
	}

	locateFarthestTile() {
		const graph = this.#mkPipeGraph();
		const startingEdges = Tile.pipes().map((pipe) => {
			const positionedTile = new PositionedTile(pipe, this.startingPosition);
			return graph.edgesAt(positionedTile) ?? { source: positionedTile, targets: [] };
		});

		const paths =
			Array.from(distinctBy(startingEdges, ({ source }) => source.asIdentifier()))
				.filter(({ targets }) => targets.length === 2)
				.map(({ source, targets: [target, otherTarget] }) => {
					const path = graph.findPath(target,
						function canVisit({ position }) {
							return !Position.equal(position, source.position);
						},
						function isTarget(positionedTile) {
							return PositionedTile.equal(positionedTile, otherTarget);
						});

					return [...(path ?? []), source];
				});

		const maxPath = maxBy(paths, comparingNumber((path) => path.length));
		if (maxPath === undefined) return [undefined, undefined];
		else return [maxPath, Math.ceil(maxPath.length / 2)];
	}

	#tileAt({ row, column }) {
		if ((row < 0) || (row >= this.gridOfTiles.length)) return undefined;
		else {
			const tiles = this.gridOfTiles[row];
			if ((column < 0) || (column >= tiles.length)) return undefined;
			else return tiles[column];
		}
	}

	#mkPipeGraph() {
		function mkConnectionKey(connection) {
			const sourceKey = connection.source.asIdentifier();
			const targetKey = connection.target.asIdentifier();
			const cmp = Position.comparingPosition()(connection.source.position, connection.target.position);
			if (cmp < 0) return `${sourceKey} to ${targetKey}`;
			else return `${targetKey} to ${sourceKey}`;
		}

		function determinePipeConnections(knownConnections, tileAt, positionedTile) {
			const potentialPipeConnections = positionedTile.potentialPipeConnections();
			return potentialPipeConnections.flatMap(({ pipes, position }) => {
				const neighbourTile = tileAt(position);
				if (neighbourTile === undefined) return [];
				else if (pipes.every((pipe) => !Tile.equal(pipe, neighbourTile))) return [];
				else {
					const connection = {
						source: positionedTile,
						target: new PositionedTile(neighbourTile, position)
					};

					const connectionKey = mkConnectionKey(connection);
					if (knownConnections.has(connectionKey)) return [];
					else {
						knownConnections.add(connectionKey);
						return [connection];
					}
				}
			});
		}

		const knownConnections = new Set();
		const allPipeConnections = [];
		const tileAt = this.#tileAt.bind(this);
		for (const [tiles, row] of zipWithIndex(this.gridOfTiles)) {
			for (const [tile, column] of zipWithIndex(tiles)) {
				const currentPosition = new Position(row, column);
				const origins = tile.isSTile() ? Tile.pipes() : [tile];
				const pipeConnections =
					origins
						.filter((origin) => origin.isPipe())
						.flatMap((origin) => {
							const positionedOriginTile = new PositionedTile(origin, currentPosition);
							return determinePipeConnections(knownConnections, tileAt, positionedOriginTile);
						});

				allPipeConnections.push(...pipeConnections);
			}
		}

		return Digraph.fromEdges(allPipeConnections);
	}
}

class Digraph {

	#internalGraph = new Map();

	constructor(internalGraph) {
		this.#internalGraph = internalGraph;
	}

	static fromEdges(edges) {
		const internalGraph = new Map();
		const seenEdgesByGraphKey = new Map();
		for (const edge of edges) {
			const sourceKey = edge.source.asIdentifier();
			const sourceEdgeKey = edge.target.asIdentifier();
			computeMapEntry(internalGraph, sourceKey, (graphKey, graphEntry) => {
				const seenSourceEdges = seenEdgesByGraphKey.get(graphKey) ?? new Set();
				if (seenSourceEdges.has(sourceEdgeKey)) return graphEntry;
				else {
					computeMapEntry(seenEdgesByGraphKey, graphKey, (_key, seenEdges) => {
						if (seenEdges === undefined) return new Set([sourceEdgeKey]);
						else return seenEdges.add(sourceEdgeKey);
					});

					const { source, targets } = graphEntry ?? { source: edge.source, targets: [] };
					return { source, targets: [...targets, edge.target] };
				}
			});

			const invertedEdge = { source: edge.target, target: edge.source };
			const invertedSourceKey = invertedEdge.source.asIdentifier();
			const invertedSourceEdgeKey = invertedEdge.target.asIdentifier();
			computeMapEntry(internalGraph, invertedSourceKey, (graphKey, graphEntry) => {
				const seenSourceEdges = seenEdgesByGraphKey.get(graphKey) ?? new Set();
				if (seenSourceEdges.has(invertedSourceEdgeKey)) return graphEntry;
				else {
					computeMapEntry(seenEdgesByGraphKey, graphKey, (_key, seenEdges) => {
						if (seenEdges === undefined) return new Set([invertedSourceEdgeKey]);
						else return seenEdges.add(invertedSourceEdgeKey);
					});

					const { source, targets } = graphEntry ?? { source: invertedEdge.source, targets: [] };
					return { source, targets: [...targets, invertedEdge.target] };
				}
			});
		}

		return new Digraph(internalGraph);
	}

	edgesAt(positionedTile) {
		return this.#internalGraph.get(positionedTile.asIdentifier());
	}

	adj(positionedTile) {
		const graphEntry = this.#internalGraph.get(positionedTile.asIdentifier());
		return graphEntry?.targets ?? [];
	}

	findPath(positionedTile, canVisit, targetPredicate) {
		function buildPath(originsByTarget, currentPositionedTile, initialPath) {
			const path = [...initialPath];
			let positionedTile = currentPositionedTile;
			while (positionedTile !== undefined) {
				path.push(positionedTile);
				positionedTile = originsByTarget.get(positionedTile.asIdentifier());
			}

			return path;
		}

		function dfs(graph, stack, state) {
			while (stack.length) {
				const currentPositionedTile = stack.pop();
				state.markedVertexKeys.add(currentPositionedTile.asIdentifier());
				for (const otherPositionedTile of graph.adj(currentPositionedTile)) {
					const neighbourVertexKey = otherPositionedTile.asIdentifier();
					if (state.path !== undefined) return;
					else if (!canVisit(otherPositionedTile)) continue;
					else if (targetPredicate(otherPositionedTile)) {
						const path = buildPath(state.originsByTarget, currentPositionedTile, [
							otherPositionedTile
						]);

						state.path = path;
					} else if (!state.markedVertexKeys.has(neighbourVertexKey)) {
						state.originsByTarget.set(neighbourVertexKey, currentPositionedTile);
						stack.push(otherPositionedTile);
					}
				}
			}
		}

		const dfsState = {
			markedVertexKeys: new Set(),
			originsByTarget: new Map(),
			path: undefined
		};

		dfs(this, [positionedTile], dfsState);
		return dfsState.path;
	}
}

class PositionedTile {

	#identifier = undefined;

	constructor(tile, position) {
		this.tile = tile;
		this.position = position;
	}

	static equal(leftPositionedTile, rightPositionedTile) {
		return Tile.equal(leftPositionedTile.tile, rightPositionedTile.tile)
			&& Position.equal(leftPositionedTile.position, rightPositionedTile.position);
	}

	potentialPipeConnections() {
		return this.tile.potentialPipeConnectionsAt(this.position);
	}

	asIdentifier() {
		if (this.#identifier !== undefined) return this.#identifier;
		else {
			this.#identifier = `PositionedTile{ tile=${this.tile.asIdentifier()}, position=${this.position.asIdentifier()} }`;
			return this.#identifier;
		}
	}
}

class Tile {

	static #KIND_PATTERN = /^[|\-LJ7F.S]$/;

	static NOSO_PIPE = new Tile("|");
	static EAWE_PIPE = new Tile("-");
	static NOEA_PIPE = new Tile("L");
	static NOWE_PIPE = new Tile("J");
	static SOWE_PIPE = new Tile("7");
	static SOEA_PIPE = new Tile("F");

	static START_TILE = new Tile("S");
	static GROUND_TILE = new Tile(".");

	#identifier = undefined;

	constructor(kind) {
		if (!Tile.#KIND_PATTERN.test(kind)) throw new Error("unknown tile.");

		this.kind = kind;
	}

	static of(kind) {
		switch (kind) {
			case "|":
				return Tile.NOSO_PIPE;
			case "-":
				return Tile.EAWE_PIPE;
			case "L":
				return Tile.NOEA_PIPE;
			case "J":
				return Tile.NOWE_PIPE;
			case "7":
				return Tile.SOWE_PIPE;
			case "F":
				return Tile.SOEA_PIPE;
			case "S":
				return Tile.START_TILE;
			case ".":
				return Tile.GROUND_TILE;
			default:
				throw new Error("unknown tile.");
		}
	}

	static equal(leftTile, rightTile) {
		if (!leftTile || !rightTile) return false;
		else return leftTile.kind === rightTile.kind;
	}

	static pipes() {
		return [
			Tile.NOSO_PIPE,
			Tile.EAWE_PIPE,
			Tile.NOEA_PIPE,
			Tile.NOWE_PIPE,
			Tile.SOWE_PIPE,
			Tile.SOEA_PIPE
		];
	}

	isSTile() {
		return Tile.equal(Tile.START_TILE, this);
	}

	isPipe() {
		return Tile.pipes().some((pipe) => Tile.equal(pipe, this));
	}

	potentialPipeConnectionsAt(position) {
		switch (this.kind) {
			case ".":
				return [];
			case "S":
				return [];
			case "|":
				return [
					{
						position: position.southward(),
						pipes: [
							Tile.NOSO_PIPE,
							Tile.NOEA_PIPE,
							Tile.NOWE_PIPE
						]
					},
					{
						position: position.northward(),
						pipes: [
							Tile.NOSO_PIPE,
							Tile.SOWE_PIPE,
							Tile.SOEA_PIPE
						]
					}
				];
			case "-":
				return [
					{
						position: position.westward(),
						pipes: [
							Tile.EAWE_PIPE,
							Tile.NOEA_PIPE,
							Tile.SOEA_PIPE
						]
					},
					{
						position: position.eastward(),
						pipes: [
							Tile.EAWE_PIPE,
							Tile.NOWE_PIPE,
							Tile.SOWE_PIPE
						]
					}
				];
			case "L":
				return [
					{
						position: position.northward(),
						pipes: [
							Tile.NOSO_PIPE,
							Tile.SOWE_PIPE,
							Tile.SOEA_PIPE
						]
					},
					{
						position: position.eastward(),
						pipes: [
							Tile.EAWE_PIPE,
							Tile.NOWE_PIPE,
							Tile.SOWE_PIPE
						]
					}
				];
			case "J":
				return [
					{
						position: position.northward(),
						pipes: [
							Tile.NOSO_PIPE,
							Tile.SOWE_PIPE,
							Tile.SOEA_PIPE
						]
					},
					{
						position: position.westward(),
						pipes: [
							Tile.EAWE_PIPE,
							Tile.NOEA_PIPE,
							Tile.SOEA_PIPE
						]
					}
				];
			case "7":
				return [
					{
						position: position.westward(),
						pipes: [
							Tile.EAWE_PIPE,
							Tile.NOEA_PIPE,
							Tile.SOEA_PIPE
						]
					},
					{
						position: position.southward(),
						pipes: [
							Tile.NOSO_PIPE,
							Tile.NOEA_PIPE,
							Tile.NOWE_PIPE
						]
					}
				];
			case "F":
				return [
					{
						position: position.southward(),
						pipes: [
							Tile.NOSO_PIPE,
							Tile.NOEA_PIPE,
							Tile.NOWE_PIPE
						]
					},
					{
						position: position.eastward(),
						pipes: [
							Tile.EAWE_PIPE,
							Tile.NOWE_PIPE,
							Tile.SOWE_PIPE
						]
					}
				];
			default:
				throw new Error("illegal state.");
		}
	}

	asIdentifier() {
		if (this.#identifier !== undefined) return this.#identifier;
		else {
			this.#identifier = `Tile{ kind=${this.kind} }`;
			return this.#identifier;
		}
	}
}

class Position {

	#identifier = undefined;

	constructor(row, column) {
		this.row = row;
		this.column = column;
	}

	static equal(leftPosition, rightPosition) {
		return (leftPosition.row === rightPosition.row)
			&& (leftPosition.column === rightPosition.column);
	}

	static comparingPosition() {
		return thenComparing(comparingNumber(({ row }) => row), comparingNumber(({ column }) => column));
	}

	northward() {
		return new Position(this.row - 1, this.column);
	}

	eastward() {
		return new Position(this.row, this.column + 1);
	}

	southward() {
		return new Position(this.row + 1, this.column);
	}

	westward() {
		return new Position(this.row, this.column - 1);
	}

	asIdentifier() {
		if (this.#identifier !== undefined) return this.#identifier;
		else {
			this.#identifier = `Position{ row=${this.row}, column=${this.column}}`;
			return this.#identifier;
		}
	}
}

// --- Day 7: Camel Cards ---

import { open } from "node:fs/promises";
import { resolve } from "node:path";
import { comparing, comparingNumber, reversedComparator, thenComparing } from "./util/comparators.js";
import { collectFirst, zipWithIndex } from "./util/iterators.js";
import { groupIntoArrayBy } from "./util/maps.js";

export async function part1(camelCardsDataUrl = resolve("resources/day07/input01.txt")) {
	const handNotes = await parseCamelCardsData(camelCardsDataUrl);
	return determineTotalWinnings(handNotes);
}

export async function part2(camelCardsDataUrl = resolve("resources/day07/input01.txt")) {
	const jokerHandNotes = await parseCamelCardsData(camelCardsDataUrl, true);
	return determineTotalWinnings(jokerHandNotes, true);
}

function determineTotalWinnings(handNotes, applyJokerRule = false) {
	const handOrdering = Hand.comparingByStrength(applyJokerRule);
	const sortedHandNotes = handNotes.sort(comparing(({ hand }) => hand, handOrdering));
	return sortedHandNotes.reduce((prevValue, { bid }, index) => {
		const rank = index + 1;
		return prevValue + (bid * rank);
	}, 0);
}

function handTypeOf(cards) {
	const cardsByCardLabel = groupIntoArrayBy(cards, ({ label }) => label);
	switch (cardsByCardLabel.size) {
		case 5: {
			const cards = Array.from(cardsByCardLabel.values()).flat();
			return new HighCard(...cards);
		}
		case 4: {
			const [pair, [secondCard], [thirdCard], [fourthCard]] =
				Array.from(cardsByCardLabel.values()).sort(reversedComparator(comparingNumber((cards) => cards.length)));
			return new OnePair(pair, secondCard, thirdCard, fourthCard);
		}
		case 1: {
			const [fives] = cardsByCardLabel.values();
			return new FiveOaK(fives);
		}
		case 2: {
			const [leftCards, rightCards] =
				Array.from(cardsByCardLabel.values()).sort(comparingNumber((cards) => cards.length));
			if ((leftCards.length === 1) && (rightCards.length === 4)) {
				return new FourOaK(rightCards, ...leftCards);
			} else if ((leftCards.length === 2) && (rightCards.length === 3)) {
				return new FullHouse(rightCards, leftCards);
			} else {
				throw new Error("illegal state.");
			}
		}
		case 3: {
			const [leftCards, middleCards, rightCards] =
				Array.from(cardsByCardLabel.values()).sort(comparingNumber((cards) => cards.length));
			if ((leftCards.length === 1) && (middleCards.length === 1) && (rightCards.length === 3)) {
				return new ThreeOaK(rightCards, ...leftCards, ...middleCards);
			} else if ((leftCards.length === 1) && (middleCards.length === 2) && (rightCards.length === 2)) {
				return new TwoPair(rightCards, middleCards, ...leftCards);
			} else {
				throw new Error("illegal state.");
			}
		}
		default:
			throw new Error("illegal state.");
	}
}

async function parseCamelCardsData(camelCardsDataUrl, applyJokerRule = false) {
	const handNotes = [];
	const camelCardsDataFileDescriptor = await open(camelCardsDataUrl);
	for await (const line of camelCardsDataFileDescriptor.readLines()) {
		const [rawHand, rawBid] = line.split(/\s+/);
		const cards = rawHand.split("").map((cardLabel) => new Card(cardLabel));
		const hand = new Hand(cards, applyJokerRule);
		const handNote = new HandNote(hand, Number.parseInt(rawBid));
		handNotes.push(handNote);
	}

	return handNotes;
}

class HandNote {

	constructor(hand, bid) {
		this.hand = hand;
		this.bid = bid;
	}
}

class Hand {

	constructor(cards, applyJokerRule = false) {
		if (cards.length !== 5) {
			throw new Error("a hand must consist of exactly five cards.");
		}

		const handType = handTypeOf(cards);
		this.cards = cards;
		this.type = applyJokerRule ? handType.withJokerRuleApplied() : handType;
	}

	static comparingByStrength(applyJokerRule = false) {
		const cardOrdering =
			Array.from({ length: 5 }, (_item, index) => index)
				.map((cardIndex) => Hand.comparingByCard(cardIndex, applyJokerRule))
				.reduce((firstCardComparator, secondCardComparator) => {
					return thenComparing(firstCardComparator, secondCardComparator);
				});

		return thenComparing(Hand.comparingByType(), cardOrdering);
	}

	static comparingByType() {
		return comparingNumber((hand) => hand.type.strength);
	}

	static comparingByCard(cardIndex, applyJokerRule = false) {
		if ((cardIndex < 0) || (cardIndex >= 5)) {
			throw new Error("invalid card index.");
		}

		return comparing((hand) => hand.cards[cardIndex], Card.comparingByStrength(applyJokerRule));
	}
}

class Card {

	static #LABEL_PATTERN = /^[AKQJT2-9]$/;

	static A = new Card("A");

	constructor(label, subject) {
		if (!Card.#LABEL_PATTERN.test(label)) {
			throw new Error(`"${label}" is not a valid card label.`);
		}

		if (subject && (label !== Card.jokerLabel)) {
			throw new Error("only jokers can act like other cards");
		}

		this.label = label;
		this.subject = subject;
	}

	static joker(subject) {
		return new Card(Card.jokerLabel, subject);
	}

	static get jokerLabel() {
		return "J";
	}

	static get highest() {
		return Card.A;
	}

	static comparingByStrength(applyJokerRule = false) {
		return comparingNumber((card) => card.strength(applyJokerRule));
	}

	strength(applyJokerRule = false) {
		switch (this.label) {
			case "A":
				return 14;
			case "K":
				return 13;
			case "Q":
				return 12;
			case "J":
				return applyJokerRule ? 1 : 11;
			case "T":
				return 10;
			default:
				return Number.parseInt(this.label);
		}
	}

	isJoker() {
		return this.label === Card.jokerLabel;
	}
}

class FiveOaK {

	constructor(fives) {
		if (fives.length !== 5) throw new Error();

		this.fives = fives;
	}

	get cards() {
		return this.fives;
	}

	get strength() {
		return 7;
	}

	/**
	 * The resulting hand type is always Five of a kind.
	 *
	 * @returns {FiveOaK} the best hand type after applying the joker rule to this hand type
	 */
	withJokerRuleApplied() {
		const cards = this.cards.map((card) => {
			return card.isJoker() ? Card.joker(Card.highest) : card;
		});

		return new FiveOaK(cards);
	}
}

class FourOaK {

	constructor(fours, otherCard) {
		if (fours.length !== 4) throw new Error();

		this.fours = fours;
		this.otherCard = otherCard;
	}

	get cards() {
		return [...this.fours, this.otherCard];
	}

	get strength() {
		return 6;
	}

	/**
	 * Possible results are:
	 *
	 *  - Four of a kind, if this hand type has no jokers.
	 *  - Five of a kind, if the four cards that share the same label are jokers, or the other card is a joker.
	 *
	 * @returns {FourOaK, FiveOaK} the best hand type after applying the joker rule to this hand type
	 */
	withJokerRuleApplied() {
		if (this.otherCard.isJoker()) {
			const oneOfFours = this.fours[0];
			const joker = Card.joker(oneOfFours);
			return new FiveOaK([joker, ...this.fours]);
		} else if (this.fours.every((card) => card.isJoker())) {
			const cards = this.fours.map((_card) => Card.joker(this.otherCard));
			return new FiveOaK([this.otherCard, ...cards]);
		} else {
			return this;
		}
	}
}

class FullHouse {

	constructor(threes, twos) {
		if ((threes.length !== 3) || (twos.length !== 2)) throw new Error();

		this.threes = threes;
		this.twos = twos;
	}

	get cards() {
		return [...this.threes, ...this.twos];
	}

	get strength() {
		return 5;
	}

	/**
	 * Possible results are:
	 *
	 *  - Full house, if this hand type has no jokers.
	 *  - Five of a kind, if either the two cards that share the same label are jokers, or the three cards that share the same label are jokers.
	 *
	 * @returns {FullHouse | FiveOaK } the best hand type after applying the joker rule to this hand type
	 */
	withJokerRuleApplied() {
		if (this.twos.every((card) => card.isJoker())) {
			const oneOfThrees = this.threes[0];
			const cards = this.twos.map((_card) => Card.joker(oneOfThrees));
			return new FiveOaK([...cards, ...this.threes]);
		} else if (this.threes.every((card) => card.isJoker())) {
			const oneOfTwos = this.twos[0];
			const cards = this.threes.map((_card) => Card.joker(oneOfTwos));
			return new FiveOaK([...cards, ...this.twos]);
		} else {
			return this;
		}
	}
}

class ThreeOaK {

	constructor(threes, secondCard, thirdCard) {
		if (threes.length !== 3) throw new Error();

		this.threes = threes;
		this.secondCard = secondCard;
		this.thirdCard = thirdCard;
	}

	get cards() {
		return [...this.threes, this.secondCard, this.thirdCard];
	}

	get strength() {
		return 4;
	}

	/**
	 * Possible results are:
	 *
	 *  - Three of a kind, if this hand type has no jokers.
	 *  - Four of a kind, if either the three cards that share the same label are jokers or one of the other cards is a joker.
	 *
	 * @returns {ThreeOaK | FourOaK} the best hand type after applying the joker rule to this hand type
	 */
	withJokerRuleApplied() {
		if (this.secondCard.isJoker()) {
			const oneOfThrees = this.threes[0];
			const joker = Card.joker(oneOfThrees);
			return new FourOaK([joker, ...this.threes], this.thirdCard);
		} else if (this.thirdCard.isJoker()) {
			const oneOfThrees = this.threes[0];
			const joker = Card.joker(oneOfThrees);
			return new FourOaK([joker, ...this.threes], this.secondCard);
		} else if (this.threes.every((card) => card.isJoker())) {
			const cmp = Card.comparingByStrength()(this.secondCard, this.thirdCard);
			const highestCard = (cmp > 0) ? this.secondCard : this.thirdCard;
			const lowestCard = (cmp < 0) ? this.secondCard : this.thirdCard;
			const cards = this.threes.map((_card) => Card.joker(highestCard));
			return new FourOaK([highestCard, ...cards], lowestCard);
		} else {
			return this;
		}
	}
}

class TwoPair {

	constructor(firstPair, secondPair, otherCard) {
		if ((firstPair.length !== 2) || (secondPair.length !== 2)) throw new Error();

		this.firstPair = firstPair;
		this.secondPair = secondPair;
		this.otherCard = otherCard;
	}

	get cards() {
		return [...this.firstPair, ...this.secondPair, this.otherCard];
	}

	get strength() {
		return 3;
	}

	/**
	 * Possible results are:
	 *
	 *  - Two pair, if this hand type has no jokers.
	 *  - Four of a kind, if two jokers form a pair.
	 *  - Full house, if the card that is not part of a pair is a joker.
	 *
	 * @returns {TwoPair | FourOaK | FullHouse} the best hand type after applying the joker rule to this hand type
	 */
	withJokerRuleApplied() {
		if (this.firstPair.every((card) => card.isJoker())) {
			const oneOfSecondPair = this.secondPair[0];
			const cards = this.firstPair.map((_card) => Card.joker(oneOfSecondPair));
			return new FourOaK([...cards, ...this.secondPair,], this.otherCard);
		} else if (this.secondPair.every((card) => card.isJoker())) {
			const oneOfFirstPair = this.firstPair[0];
			const cards = this.secondPair.map((_card) => Card.joker(oneOfFirstPair));
			return new FourOaK([...this.firstPair, ...cards], this.otherCard);
		} else if (this.otherCard.isJoker()) {
			const oneOfFirstPair = this.firstPair[0];
			const oneOfSecondPair = this.secondPair[0];
			const cmp = Card.comparingByStrength()(oneOfFirstPair, oneOfSecondPair);
			const highestPair = (cmp > 0) ? this.firstPair : this.secondPair;
			const lowestPair = (cmp < 0) ? this.firstPair : this.secondPair;
			const joker = Card.joker(highestPair[0]);
			return new FullHouse([joker, ...highestPair], lowestPair);
		} else {
			return this;
		}
	}
}

class OnePair {

	constructor(pair, secondCard, thirdCard, fourthCard) {
		if (pair.length !== 2) throw new Error();

		this.pair = pair;
		this.secondCard = secondCard;
		this.thirdCard = thirdCard;
		this.fourthCard = fourthCard;
	}

	get cards() {
		return [...this.pair, this.secondCard, this.thirdCard, this.fourthCard];
	}

	get strength() {
		return 2;
	}

	/**
	 * Possible results are:
	 *
	 *  - One pair, if this hand type has no joker.
	 *  - Three of a kind, if the pair consists of jokers, or one of the other cards is a joker.
	 *
	 * @returns {OnePair | ThreeOaK } the best hand type after applying the joker rule to this hand type
	 */
	withJokerRuleApplied() {
		if (this.pair.every((card) => card.isJoker())) {
			const [highestCard, secondCard, thirdCard] =
				[this.secondCard, this.thirdCard, this.fourthCard].sort(reversedComparator(Card.comparingByStrength()));
			const cards = this.pair.map((_card) => Card.joker(highestCard));
			return new ThreeOaK([highestCard, ...cards], secondCard, thirdCard);
		} else if (this.secondCard.isJoker()) {
			const oneOfPair = this.pair[0];
			const joker = Card.joker(oneOfPair);
			return new ThreeOaK([joker, ...this.pair], this.thirdCard, this.fourthCard);
		} else if (this.thirdCard.isJoker()) {
			const oneOfPair = this.pair[0];
			const joker = Card.joker(oneOfPair);
			return new ThreeOaK([joker, ...this.pair], this.secondCard, this.fourthCard);
		} else if (this.fourthCard.isJoker()) {
			const oneOfPair = this.pair[0];
			const joker = Card.joker(oneOfPair);
			return new ThreeOaK([joker, ...this.pair], this.secondCard, this.thirdCard);
		} else {
			return this;
		}
	}
}

class HighCard {

	constructor(firstCard, secondCard, thirdCard, fourthCard, fifthsCard) {
		this.firstCard = firstCard;
		this.secondCard = secondCard;
		this.thirdCard = thirdCard;
		this.fourthCard = fourthCard;
		this.fifthsCard = fifthsCard;
	}

	get cards() {
		return [this.firstCard, this.secondCard, this.thirdCard, this.fourthCard, this.fifthsCard];
	}

	get strength() {
		return 1;
	}

	/**
	 * Possible results are:
	 *
	 *  - High card, if this hand type has no joker.
	 *  - One pair, if this hand type has one joker.
	 *
	 * @returns {HighCard | OnePair} the best hand type after applying the joker rule to this hand type
	 */
	withJokerRuleApplied() {
		const cards = this.cards.sort(reversedComparator(Card.comparingByStrength()));
		const jokerIndex = cards.findIndex((card) => card.isJoker());
		if (jokerIndex < 0) return this;
		else {
			const highestCardIndex = collectFirst(zipWithIndex(cards), ([_card, cardIndex]) => {
				return (cardIndex === jokerIndex) ? undefined : cardIndex;
			});

			const highestCard = cards[highestCardIndex];
			const joker = Card.joker(cards[highestCardIndex]);
			const otherCards = cards.filter((_card, cardIndex) => {
				return (cardIndex !== highestCardIndex) && (cardIndex !== jokerIndex);
			});

			return new OnePair([highestCard, joker], ...otherCards);
		}
	}
}

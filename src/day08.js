// --- Day 8: Haunted Wasteland ---

import { open } from "node:fs/promises";
import { resolve } from "node:path";
import { repeatForever } from "./util/iterators.js";
import { groupBy } from "./util/maps.js";

export async function part1(documentsUrl = resolve("resources/day08/input01.txt")) {
	const [network, instructions] = await parseDocuments(documentsUrl);
	const startLink = network.singleStartLink();
	const stepCount = calculateStepCount(network, instructions, startLink, ({ label }) => {
		return label === NetworkNode.TARGET_LABEL;
	});

	return stepCount;
}

export async function part2(documentsUrl = resolve("resources/day08/input01.txt")) {
	const [network, instructions] = await parseDocuments(documentsUrl);
	const stepCounts = network.startLinks().map((startLink) => {
		return calculateStepCount(network, instructions, startLink, ({ label }) => {
			return label.endsWith("Z");
		});
	});

	return stepCounts.reduce((a, b) => lcm(a, b));
}

function calculateStepCount(network, instructions, startLink, targetConditionPredicate) {
	let stepCount = 0;
	let currentLink = startLink;
	for (const instruction of repeatForever(() => instructions)) {
		if (targetConditionPredicate(currentLink.sourceNode)) break;
		else {
			const nextNode = instruction.selectNode(currentLink.targets);
			currentLink = network.getLink(nextNode);
			stepCount++;
		}
	}

	return stepCount;
}

function lcm(a, b) {
	if ((a === 0) && (b === 0)) return 0;
	else return Math.abs(a) * (Math.abs(b) / gcd(a, b));
}

function gcd(a, b) {
	if (b === 0) return a;
	else {
		const r = ((a % b) + b) % b;
		return gcd(b, r);
	}
}

async function parseDocuments(documentsUrl) {
	const networkLinePattern = /(?<src>[A-Z0-9]{3})\s*=\s*\((?<left>[A-Z0-9]{3}),\s*(?<right>[A-Z0-9]{3})\)/;

	function parseNetworkLine(networkLine) {
		const networkLineMatch = networkLine.match(networkLinePattern);
		if (networkLineMatch === null) throw new Error("invalid network pattern.");

		const [_networkLine, srcElement, leftElement, rightElement] = networkLineMatch;
		return new NetworkLink(
			new NetworkNode(srcElement),
			new NetworkNode(leftElement),
			new NetworkNode(rightElement)
		);
	}

	async function readAllLines() {
		const documentsFileDescriptor = await open(documentsUrl);
		const documentLines = [];
		for await (const line of documentsFileDescriptor.readLines()) {
			if (line.length) {
				documentLines.push(line);
			}
		}

		return documentLines;
	}

	const [instructionsLine, ...networkLines] = await readAllLines();
	const instructions = instructionsLine.split("").map((instruction) => {
		return new LRInstruction(instruction);
	});

	const network = new Network(networkLines.map(parseNetworkLine));
	return [network, instructions];
}

class Network {

	constructor(networkLinks) {
		this.networkLinksBySourceLabel = groupBy(networkLinks, ({ sourceNode }) => {
			return sourceNode.label;
		});
	}

	getLink({ label }) {
		return this.networkLinksBySourceLabel.get(label);
	}

	singleStartLink() {
		return this.networkLinksBySourceLabel.get(NetworkNode.START_LABEL);
	}

	startLinks() {
		return Array.from(this.networkLinksBySourceLabel.entries())
			.filter(([sourceLabel, _link]) => sourceLabel.endsWith("A"))
			.map(([_sourceLabel, link]) => link);
	}
}

class NetworkLink {

	constructor(sourceNode, leftNode, rightNode) {
		this.sourceNode = sourceNode;
		this.leftNode = leftNode;
		this.rightNode = rightNode;
	}

	get targets() {
		return [this.leftNode, this.rightNode];
	}
}

class NetworkNode {

	static #LABEL_PATTERN = /^[A-Z0-9]{3}$/;

	static START_LABEL = "AAA";
	static TARGET_LABEL = "ZZZ";

	constructor(label) {
		if (!NetworkNode.#LABEL_PATTERN.test(label)) throw new Error("illegal label.");

		this.label = label;
	}
}

class LRInstruction {

	static #KIND_PATTERN = /^[LR]$/;

	constructor(kind) {
		if (!LRInstruction.#KIND_PATTERN.test(kind)) throw new Error("illegal instruction.");

		this.kind = kind;
	}

	selectNode([leftNode, rightNode]) {
		switch (this.kind) {
			case "L":
				return leftNode;
			case "R":
				return rightNode;
			default:
				throw new Error("illegal state.");
		}
	}
}

// --- Day 11: Cosmic Expansion ---

import { open } from "node:fs/promises";
import { resolve } from "node:path";
import { comparingNumber } from "./util/comparators.js";
import { count, sumOf, zipWithIndex } from "./util/iterators.js";

const EMPTY_SPACE = Object.freeze({
	get isEmptySpace() {
		return true;
	}
});

const GALAXY = Object.freeze({
	get isEmptySpace() {
		return false;
	}
});

export async function part1(imageUrl = resolve("resources/day11/input01.txt")) {
	const parsedImage = await parseImage(imageUrl);
	return sumOf(galaxyDistances(parsedImage));
}

export async function part2(imageUrl = resolve("resources/day11/input01.txt"), expansionFactor = 1_000_000) {
	const parsedImage = await parseImage(imageUrl);
	return sumOf(galaxyDistances(parsedImage, expansionFactor));
}

function* galaxyDistances(image, expansionFactor = 2) {
	for (const [galaxyPosition, otherGalaxyPosition] of image.determineGalaxyPositionPairs()) {
		const [p, q] = expandPosition(image, galaxyPosition, otherGalaxyPosition, expansionFactor);
		yield p.distanceTo(q);
	}
}

function expandPosition(image, position, otherPosition, expansionFactor) {
	const hline = new LineSegment(position, new Position(position.row, otherPosition.column));
	const clength = count(image.emptyColumns, (emptyColumnSegment) => {
		return emptyColumnSegment.intersectsWith(hline);
	}) * (expansionFactor - 1);

	const vline = new LineSegment(position, new Position(otherPosition.row, position.column));
	const rlength = count(image.emptyRows, (emptyRowSegment) => {
		return emptyRowSegment.intersectsWith(vline);
	}) * (expansionFactor - 1);

	const vcmp = Position.comparingRow(position, otherPosition);
	const [p, q] = (vcmp > 0) ? [position, otherPosition] : [otherPosition, position];

	const hcmp = Position.comparingColumn(p, q);
	if (hcmp < 0) return [new Position(p.row + rlength, p.column - clength), q];
	else return [new Position(p.row + rlength, p.column + clength), q];
}

async function parseImage(imageUrl) {
	const imageGrid = [];
	const imageFileHandle = await open(imageUrl);
	for await (const imageRow of imageFileHandle.readLines()) {
		const parsedImageRow =
			Array.from(imageRow).reduce((prevParsedImageRow, imageColumn) => {
				if (imageColumn === ".") return [...prevParsedImageRow, EMPTY_SPACE];
				else if (imageColumn === "#") return [...prevParsedImageRow, GALAXY];
				else return prevParsedImageRow;
			}, []);

		imageGrid.push(parsedImageRow);
	}

	return new Image(imageGrid);
}

class Image {

	#imageGrid = [];
	#emptyRows = [];
	#emptyColumns = [];

	constructor(imageGrid) {
		this.#imageGrid = imageGrid;
		this.#emptyRows = this.#determineEmptyRows();
		this.#emptyColumns = this.#determineEmptyColumns();
	}

	get emptyRows() {
		return this.#emptyRows;
	}

	get emptyColumns() {
		return this.#emptyColumns;
	}

	*determineGalaxyPositionPairs() {
		const galaxyPositions = Array.from(this.#collectGalaxyPositions());
		for (let i = 0; i < galaxyPositions.length; i++) {
			const galaxyPosition = galaxyPositions[i];
			for (let j = i + 1; j < galaxyPositions.length; j++) {
				const otherGalaxyPosition = galaxyPositions[j];
				yield [galaxyPosition, otherGalaxyPosition];
			}
		}
	}

	*#collectGalaxyPositions() {
		for (const [row, ri] of zipWithIndex(this.#imageGrid)) {
			for (const [data, ci] of zipWithIndex(row)) {
				if (!data.isEmptySpace) yield new Position(ri, ci);
			}
		}
	}

	#determineEmptyRows() {
		return this.#imageGrid.flatMap((row, ri) => {
			if (row.some((data) => !data.isEmptySpace)) return [];
			else {
				const lineStart = new Position(ri, 0);
				const lineEnd = new Position(ri, row.length - 1);
				return [new LineSegment(lineStart, lineEnd)];
			}
		});
	}

	#determineEmptyColumns() {
		const [firstRow, ...otherRows] = this.#imageGrid;
		const initialColumnInfo = firstRow.map((data) => data.isEmptySpace);
		const columnInfo = otherRows.reduce((prevColumnInfo, row) => {
			return prevColumnInfo.map((prevEmptySpace, ci) => {
				return prevEmptySpace && row[ci].isEmptySpace;
			});
		}, initialColumnInfo);

		const end = this.#imageGrid.length - 1;
		return columnInfo.flatMap((onlyEmptySpace, ci) => {
			if (!onlyEmptySpace) return [];
			else {
				const lineStart = new Position(0, ci);
				const lineEnd = new Position(end, ci);
				return [new LineSegment(lineStart, lineEnd)];
			}
		});
	}
}

class LineSegment {

	constructor(start, end) {
		this.start = start;
		this.end = end;
	}

	isHSegment() {
		const { start: { row: startRow, column: startColumn }, end: { row: endRow, column: endColumn } } = this;
		return (startRow === endRow) && (startColumn !== endColumn);
	}

	isVSegment() {
		const { start: { row: startRow, column: startColumn }, end: { row: endRow, column: endColumn } } = this;
		return (startColumn === endColumn) && (startRow !== endRow);
	}

	includes(position) {
		const { row, column } = position;
		if (this.isHSegment()) {
			const { start: { row: r, column: c1 }, end: { column: c2 } } = this;
			return (r === row)
				&& (((c1 <= column) && (c2 >= column))
					|| ((c2 <= column) && (c1 >= column)));
		} else {
			const { start: { column: c, row: r1 }, end: { row: r2 } } = this;
			return (c === column)
				&& (((r1 <= row) && (r2 >= row))
					|| ((r2 <= row) && (r1 >= row)));
		}
	}

	intersectsWith(otherLineSegment) {
		if (this.isHSegment() && otherLineSegment.isHSegment()) {
			const { start: { column: l1Start }, end: { column: l1End } } = this;
			const { start: { column: l2Start }, end: { column: l2End } } = otherLineSegment;
			return !((l2End < l1Start) || (l2Start > l1End))
				|| ((l1End < l2Start) || (l1Start > l2End));
		} else if (this.isVSegment() && otherLineSegment.isVSegment()) {
			const { start: { row: l1Start }, end: { row: l1End } } = this;
			const { start: { row: l2Start }, end: { row: l2End } } = otherLineSegment;
			return ((l2End < l1Start) || (l2Start > l1End))
				|| ((l1End < l2Start) || (l1Start > l2End));
		} else {
			const row = this.isHSegment() ? this.start.row : otherLineSegment.start.row;
			const column = this.isVSegment() ? this.start.column : otherLineSegment.start.column;
			const p = new Position(row, column);
			return this.includes(p) && otherLineSegment.includes(p);
		}
	}
}

class Position {

	constructor(row, column) {
		this.row = row;
		this.column = column;
	}

	static get comparingRow() {
		return comparingNumber(({ row }) => row);
	}

	static get comparingColumn() {
		return comparingNumber(({ column }) => column);
	}

	/**
	 * Calculates the taxicab distance between this position and the given other position.
	 */
	distanceTo(otherPosition) {
		const { row: y1, column: x1 } = this;
		const { row: y2, column: x2 } = otherPosition;
		return Math.abs(x1 - x2) + Math.abs(y1 - y2);
	}
}

// --- Day 9: Mirage Maintenance ---

import { open } from "node:fs/promises";
import { resolve } from "node:path";
import { slidingWindow } from "./util/iterators.js";

export async function run(reportUrl = resolve("resources/day09/input01.txt")) {
	const report = await parseReport(reportUrl);
	return report.sumOfExtrapolatedValues();
}

export async function part1(reportUrl) {
	const [_sumOfExtrapolatedStartValues, sumOfExtrapolatedEndValues] = await run(reportUrl);
	return sumOfExtrapolatedEndValues;
}

export async function part2(reportUrl) {
	const [sumOfExtrapolatedStartValues, _sumOfExtrapolatedEndValues] = await run(reportUrl);
	return sumOfExtrapolatedStartValues;
}

async function parseReport(reportUrl) {
	const allHistoryValues = [];
	const reportFileHandle = await open(reportUrl);
	for await (const line of reportFileHandle.readLines()) {
		const historyValues = line.split(/\s+/).map((value) => Number.parseInt(value));
		allHistoryValues.push(historyValues);
	}

	return new Report(allHistoryValues);
}

class Report {

	constructor(allHistoryValues) {
		this.allHistoryValues = allHistoryValues;
	}

	sumOfExtrapolatedValues() {
		const extrapolatedValues = this.allHistoryValues.map((historyValues) => {
			return this.#extrapolateValues(historyValues);
		});

		return extrapolatedValues.reduce(([prevStartSum, prevEndSum], [startValue, endValue]) => {
			return [prevStartSum + startValue, prevEndSum + endValue];
		}, [0, 0]);
	}

	#extrapolateValues(historyValues) {
		function go(historySequences, currHistoryValues) {
			if (currHistoryValues.every((value) => value === 0)) return historySequences;
			else {
				const historyValuePairs =
					Array.from(slidingWindow(currHistoryValues, 2)).filter((values) => {
						return values.length === 2;
					});

				const diffValues =
					historyValuePairs.reduce((prevDiffValues, [prevValue, currValue]) => {
						prevDiffValues.push(currValue - prevValue);
						return prevDiffValues;
					}, []);

				return go([...historySequences, diffValues], diffValues);
			}
		}

		const historySequences = go([historyValues], historyValues);
		return historySequences.reduceRight(([prevStartValue, prevEndValue], currSequence) => {
			const currStartValue = currSequence.at(0);
			const currEndValue = currSequence.at(-1);
			return [currStartValue - prevStartValue, currEndValue + prevEndValue];
		}, [0, 0]);
	}
}

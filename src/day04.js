// --- Day 4: Scratchcards ---

import { open } from "node:fs/promises";
import { resolve } from "node:path";
import { sumOf, windowMap } from "./util/iterators.js";
import { computeMapEntry } from "./util/maps.js";

export async function readAllScratchcards(scratchcardTableUrl = resolve("resources/day04/input01.txt")) {
	return await parseScratchcards(scratchcardTableUrl);
}

export function part1(scratchcards) {
	return scratchcards
		.map((scratchcard) => scratchcard.calculatePoints())
		.filter((points) => !Number.isNaN(points))
		.reduce((prevPoints, points) => prevPoints + points);
}

export function part2(scratchcards) {
	return processScratchcards(scratchcards);
}

function processScratchcards(scratchcards) {
	function* collectCardIds(startIndex, endIndex) {
		for (let i = startIndex; i < endIndex && i < scratchcards.length; i++) {
			const { cardId } = scratchcards[i];
			yield cardId;
		}
	}

	const numberOfCopiesByCardId = new Map();
	for (let i = 0, nextCardIndex = 1; i < scratchcards.length; i++, nextCardIndex++) {
		const scratchcard = scratchcards[i];
		const matchingNumberCount = scratchcard.matchingNumberCount;
		const currentNumberOfCopies = numberOfCopiesByCardId.get(scratchcard.cardId) ?? 0;
		for (const copyCardId of collectCardIds(nextCardIndex, nextCardIndex + matchingNumberCount)) {
			computeMapEntry(numberOfCopiesByCardId, copyCardId, (_cardId, numberOfCopies) => {
				return (numberOfCopies ?? 0) + 1 + currentNumberOfCopies;
			});
		}
	}

	return scratchcards.length + sumOf(numberOfCopiesByCardId.values());
}

async function parseScratchcards(scratchcardTableUrl) {
	function* collectNumbers(formattedNumbers) {
		const numberWindows =
			windowMap(formattedNumbers,
				function openWhen(character) {
					const openWindow = character !== " ";
					const digit = openWindow ? Number.parseInt(character) : undefined;
					return { openWindow, item: digit };
				},
				function closeWhen(character) {
					return character === " ";
				},
				function transformCharacter(character) {
					return Number.parseInt(character);
				});

		for (const numberWindow of numberWindows) {
			yield numberWindow.reduce((prevNumberPart, digit) => prevNumberPart * 10 + digit);
		}
	}

	const scratchcards = [];
	const scratchcardPattern = /Card\s+(?<cardId>\d+):\s*(?<winningNumbers>(?:\d+\s*)+)\s*\|\s*(?<ownNumbers>(?:\d+\s*)+)/;
	const scratchcardTableFileDescriptor = await open(scratchcardTableUrl);
	for await (let scratchcardLine of scratchcardTableFileDescriptor.readLines()) {
		const scratchcardMatch = scratchcardLine.match(scratchcardPattern);
		if (!scratchcardMatch) {
			throw new Error(`Invalid scratchcard pattern: ${scratchcardLine}`);
		}

		const [_line, rawCardId, rawWinningNumbers, rawOwnNumbers] = scratchcardMatch;
		const winningNumbers = new Set(collectNumbers(rawWinningNumbers));
		const ownNumbers = Array.from(collectNumbers(rawOwnNumbers));
		scratchcards.push(new Scratchcard(Number.parseInt(rawCardId), winningNumbers, ownNumbers));
	}

	return scratchcards;
}

class Scratchcard {

	#matchingNumbers = [];

	constructor(cardId, winningNumbers, ownNumbers) {
		this.cardId = Number(cardId);
		this.winningNumbers = winningNumbers;
		this.ownNumbers = ownNumbers;

		this.#matchingNumbers = this.#collectMatchingNumbers();
	}

	get matchingNumberCount() {
		return this.#matchingNumbers.length;
	}

	calculatePoints() {
		return this.#matchingNumbers.reduce((prevPoints) => {
			if (Number.isNaN(prevPoints)) return 1;
			else return prevPoints * 2;
		}, Number.NaN);
	}

	#collectMatchingNumbers() {
		return this.ownNumbers.filter((ownNumber) => {
			return this.winningNumbers.has(ownNumber);
		});
	}
}

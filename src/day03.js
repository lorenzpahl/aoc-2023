// --- Day 3: Gear Ratios ---

import { open } from "node:fs/promises";
import { resolve } from "node:path";
import { sumOf, windowUntil } from "./util/iterators.js";
import { groupIntoSetBy } from "./util/maps.js";

const DIGIT_PATTERN = /\d/;

const SchematicElement = Object.freeze({
	PERIOD: 0,
	SYMBOL: 1,
	DIGIT: 2
});

export async function createEngineSchematic(engineSchematicUrl = resolve("resources/day03/input01.txt")) {
	return await parseEngineSchematic(engineSchematicUrl);
}

export function part1(engineSchematic) {
	const partNumbers = engineSchematic.partNumbers();
	return sumOf([...partNumbers].map(({ value: partNumber }) => partNumber));
}

export function part2(engineSchematic) {
	return sumOf(engineSchematic.gears().map((gear) => gear.gearRatio));
}

async function parseEngineSchematic(engineSchematicUrl) {
	function parseSchematicElement(character) {
		if (character === ".") return { schematicElement: SchematicElement.PERIOD, element: null };
		else if (DIGIT_PATTERN.test(character)) return { schematicElement: SchematicElement.DIGIT, element: new Digit(Number.parseInt(character)) };
		else return { schematicElement: SchematicElement.SYMBOL, element: new Symbol(character) };
	}

	function processSchematicElements(row, schematicElements) {
		let column = 0;
		const schematicRow = [];
		const schematicNumbers = [];
		for (let schematicElementWindow of schematicElements) {
			const rawSchematicNumber =
				schematicElementWindow.reduce((prevNumberPart, { schematicElement, element }) => {
					if (schematicElement !== SchematicElement.DIGIT) return "";
					else return prevNumberPart + element.value;
				}, "");

			if (rawSchematicNumber.length) {
				const schematicNumber = new SchematicNumber(Number.parseInt(rawSchematicNumber), row, column, column + rawSchematicNumber.length);
				schematicNumbers.push(schematicNumber);
			}

			schematicRow.push(...schematicElementWindow);
			column += schematicElementWindow.length;
		}

		return [schematicRow, schematicNumbers];
	}

	let row = 0;
	const schematic = [];
	const schematicNumbersByIdentifier = new Map();
	const engineSchematicFileDescriptor = await open(engineSchematicUrl);
	for await (let schematicLine of engineSchematicFileDescriptor.readLines()) {
		const schematicElements =
			windowUntil(schematicLine, function closeWhen(character, prevItem) {
				const { schematicElement } = parseSchematicElement(character);
				return prevItem.schematicElement !== schematicElement;
			}, parseSchematicElement);

		const [schematicRow, schematicNumbers] = processSchematicElements(row, schematicElements);
		schematic.push(schematicRow);
		for (let schematicNumber of schematicNumbers) {
			for (let schematicIdentifier of schematicNumber.schematicIdentifiers()) {
				schematicNumbersByIdentifier.set(schematicIdentifier.asMapKey, schematicNumber);
			}
		}

		row++;
	}

	return new EngineSchematic(schematic, schematicNumbersByIdentifier);
}

export class EngineSchematic {

	#schematic = [];
	#schematicNumbersByIdentifier = new Map();

	constructor(schematic, schematicNumbersByIdentifier) {
		this.#schematic = schematic;
		this.#schematicNumbersByIdentifier = schematicNumbersByIdentifier;
	}

	gears() {
		function* flattenPartNumbers(schematicNumbers) {
			for (let [gearIdentifier, partNumbers] of schematicNumbers) {
				for (let partNumber of partNumbers) {
					yield [gearIdentifier, partNumber];
				}
			}
		}

		const schematicNumbers = this.#collectAdjacentSchematicNumbers(({ element }) => {
			return element?.value === Gear.SYMBOL.value;
		});

		const partNumbersByGear =
			groupIntoSetBy(flattenPartNumbers(schematicNumbers),
				function classify([gearIdentifier, _partNumber]) {
					return gearIdentifier;
				},
				function transformItem([_gearIdentifier, partNumber]) {
					return partNumber;
				}
			);

		return Array.from(partNumbersByGear.entries())
			.filter(([_gearIdentifier, partNumbers]) => partNumbers.size === 2)
			.map(([gearIdentifier, partNumbers]) => {
				const [firstPartNumber, secondPartNumber] = partNumbers;
				return new Gear(gearIdentifier, firstPartNumber, secondPartNumber);
			});
	}

	partNumbers() {
		const schematicNumbers = this.#collectAdjacentSchematicNumbers(({ schematicElement }) => {
			return schematicElement === SchematicElement.SYMBOL;
		});

		return new Set(Array.from(schematicNumbers).flatMap(([_symbolIdentifier, partNumbers]) => [...partNumbers]));
	}

	*#collectAdjacentSchematicNumbers(schematicElementPredicate) {
		const schematic = this.#schematic;
		const schematicRowCount = schematic.length;
		for (let ri = 0; ri < schematicRowCount; ri++) {
			const schematicRow = schematic[ri];
			const schematicColumnCount = schematicRow.length;
			for (let ci = 0; ci < schematicColumnCount; ci++) {
				const schematicElementDescriptor = schematicRow[ci];
				if (schematicElementPredicate(schematicElementDescriptor)) {
					const schematicIdentifier = new SchematicIdentifier(ri, ci);
					const adjacentSchematicNumbers = new Set(this.#adjacentSchematicNumbers(schematicIdentifier));
					yield [schematicIdentifier, adjacentSchematicNumbers];
				}
			}
		}
	}

	*#adjacentSchematicNumbers({ row, column }) {
		for (let ri = row - 1; ri <= row + 1; ri++) {
			for (let ci = column - 1; ci <= column + 1; ci++) {
				if ((ri === row) && (ci === column)) continue;

				const schematicNumber = this.#schematicNumberAt(new SchematicIdentifier(ri, ci));
				if (schematicNumber) yield schematicNumber;
			}
		}
	}

	#schematicNumberAt(schematicIdentifier) {
		return this.#schematicNumbersByIdentifier.get(schematicIdentifier.asMapKey);
	}
}

class SchematicNumber {

	constructor(value, row, startIndex, endIndex) {
		if (!Number.isInteger(value)) {
			throw new Error(`${value} is not a schematic number.`);
		}

		this.value = Number(value);
		this.row = Number(row);
		this.startIndex = Number(startIndex);
		this.endIndex = Number(endIndex);
	}

	*schematicIdentifiers() {
		for (let ci = this.startIndex; ci < this.endIndex; ci++) {
			yield new SchematicIdentifier(this.row, ci);
		}
	}
}

class Digit {

	constructor(value) {
		if (!DIGIT_PATTERN.test(String(value)) || !Number.isInteger(value)) {
			throw new Error(`${value} ist not a single digit.`);
		}

		this.value = Number(value);
	}

	toString() {
		return String(this.value);
	}
}

class Symbol {

	constructor(value) {
		if (!Symbol.isSymbol(value)) {
			throw new Error(`${value} is not a symbol.`);
		}

		this.value = value;
	}

	static isSymbol(character) {
		return (character !== ".") && !DIGIT_PATTERN.test(character);
	}

	toString() {
		return this.value;
	}
}

class Gear {

	static SYMBOL = new Symbol("*");

	constructor(schematicIdentifier, firstPartNumber, secondPartNumber) {
		this.schematicIdentifier = schematicIdentifier;
		this.firstPartNumber = firstPartNumber;
		this.secondPartNumber = secondPartNumber;
	}

	get gearRatio() {
		return this.firstPartNumber.value * this.secondPartNumber.value;
	}
}

class SchematicIdentifier {

	constructor(row, column) {
		this.row = Number(row);
		this.column = Number(column);
	}

	get asMapKey() {
		return `r${this.row}c${this.column}`;
	}
}

// --- Day 2: Cube Conundrum ---

import { open } from "node:fs/promises";
import { resolve } from "node:path";
import { sumOf } from "./util/iterators.js";

const CubeColor = Object.freeze({
	RED: "red",
	GREEN: "green",
	BLUE: "blue"
});

export async function part1(
	gameRequirement = new GameRequirement(12, 13, 14),
	gameRecordUrl = resolve("resources/day02/input01.txt")
) {
	const gameRecords = await parseGameRecords(gameRecordUrl);
	const gameIds =
		gameRecords
			.filter(isGamePossible.bind(undefined, gameRequirement))
			.map(({ gameId }) => gameId);

	return sumOf(gameIds);
}

export async function part2(gameRecordUrl = resolve("resources/day02/input01.txt")) {
	function minimumCubeSetOf({ redCubeCount: prevRedCubeCount, greenCubeCount: prevGreenCubeCount, blueCubeCount: prevBlueCubeCount }, cubeSample) {
		const redCubeCount = cubeSample.getCubeCountByColor(CubeColor.RED);
		const greenCubeCount = cubeSample.getCubeCountByColor(CubeColor.GREEN);
		const blueCubeCount = cubeSample.getCubeCountByColor(CubeColor.BLUE);
		return {
			redCubeCount: (redCubeCount > prevRedCubeCount) ? redCubeCount : prevRedCubeCount,
			greenCubeCount: (greenCubeCount > prevGreenCubeCount) ? greenCubeCount : prevGreenCubeCount,
			blueCubeCount: (blueCubeCount > prevBlueCubeCount) ? blueCubeCount : prevBlueCubeCount
		};
	}

	const gameRecords = await parseGameRecords(gameRecordUrl);
	const powers = gameRecords.map((gameRecord) => {
		const { redCubeCount: minRedCubeCount, greenCubeCount: minGreenCubeCount, blueCubeCount: minBlueCubeCount } =
			gameRecord.cubeSamples.reduce(minimumCubeSetOf, { redCubeCount: 0, greenCubeCount: 0, blueCubeCount: 0 });

		return minRedCubeCount * minGreenCubeCount * minBlueCubeCount;
	});

	return sumOf(powers);
}

function isGamePossible(gameRequirement, gameRecord) {
	return gameRecord.cubeSamples.every((cubeSample) => {
		return cubeSample.cubeSelections.every((cubeSelection) => {
			const selectedCubeColor = cubeSelection.cubeColor;
			const selectedCubeCount = cubeSelection.cubeCount;
			return selectedCubeCount <= gameRequirement.cubeCountByColor(selectedCubeColor);
		});
	});
}

async function parseGameRecords(gameRecordUrl) {
	const gameRecordPattern = /Game (?<gameId>\d+): (?<cubeSamples>.+)/;
	const cubeSelectionPattern = /(?<cubeCount>\d+) (?<cubeColor>red|green|blue)/;

	function parseCubeSelection(formattedCubeSelection) {
		const cubeSelectionMatch = formattedCubeSelection.trim().match(cubeSelectionPattern);
		if (cubeSelectionMatch === null) {
			throw new Error("Invalid cube selection pattern.");
		}

		const [_matchedCubeSelection, cubeCount, cubeColor] = cubeSelectionMatch;
		return new CubeSelection(cubeColor, cubeCount);
	}

	function parseCubeSample(formattedCubeSample) {
		const cubeSelections = formattedCubeSample.split(",").map(parseCubeSelection);
		return new CubeSample(cubeSelections);
	}

	const gameRecords = [];
	const gameRecordFileDescriptor = await open(gameRecordUrl);
	for await (let formattedGameRecord of gameRecordFileDescriptor.readLines()) {
		const gameRecordMatch = formattedGameRecord.match(gameRecordPattern);
		if (gameRecordMatch === null) {
			throw new Error("Invalid game record pattern.");
		}

		const [_matchedRecord, matchedGameId, matchedCubeSamples] = gameRecordMatch;
		const cubeSamples = matchedCubeSamples.split(";").map(parseCubeSample);
		const gameRecord = new GameRecord(matchedGameId, cubeSamples);
		gameRecords.push(gameRecord);
	}

	return gameRecords;
}

class GameRequirement {

	#cubeCountsByColor = new Map();

	constructor(redCubeCount, greenCubeCount, blueCubeCount) {
		this.#cubeCountsByColor.set(CubeColor.RED, Number(redCubeCount));
		this.#cubeCountsByColor.set(CubeColor.GREEN, Number(greenCubeCount));
		this.#cubeCountsByColor.set(CubeColor.BLUE, Number(blueCubeCount));
	}

	cubeCountByColor(cubeColor) {
		return this.#cubeCountsByColor.get(cubeColor);
	}
}

class GameRecord {

	constructor(gameId, cubeSamples) {
		this.gameId = Number(gameId);
		this.cubeSamples = cubeSamples;
	}
}

class CubeSample {

	#cubeSelectionsByColor = new Map();

	constructor(cubeSelections) {
		for (let cubeSelection of cubeSelections) {
			this.#cubeSelectionsByColor.set(cubeSelection.cubeColor, cubeSelection);
		}
	}

	get cubeSelections() {
		return Array.from(this.#cubeSelectionsByColor.values());
	}

	getCubeCountByColor(cubeColor) {
		const cubeSelection = this.#cubeSelectionsByColor.get(cubeColor);
		return cubeSelection?.cubeCount ?? 0;
	}
}

class CubeSelection {

	constructor(cubeColor, cubeCount) {
		this.cubeColor = cubeColor;
		this.cubeCount = Number(cubeCount);
	}
}

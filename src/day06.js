// --- Day 6: Wait For It ---

import { open } from "node:fs/promises";
import { resolve } from "node:path";
import { productOf, zip } from "./util/iterators.js";

export async function part1(boatRaceRecordUrl = resolve("resources/day06/input01.txt")) {
	const raceRecords = await parseBoatRaceRecord(boatRaceRecordUrl);
	return productOf(raceRecords.map((raceRecord) => calculateNumberOfWaysToBeat(raceRecord)));
}

export async function part2(boatRaceRecordUrl = resolve("resources/day06/input01.txt")) {
	const [raceRecord] = await parseBoatRaceRecord(boatRaceRecordUrl, true);
	return calculateNumberOfWaysToBeat(raceRecord);
}

function calculateNumberOfWaysToBeat(raceRecord) {
	function isBetter(x) {
		return (raceRecord.raceTime - x) * x > raceRecord.bestDistance;
	}

	let bestMinValue = undefined;
	for (let x = 1; x < raceRecord.raceTime; x++) {
		if (isBetter(x)) {
			bestMinValue = x;
			break;
		}
	}

	let bestMaxValue = undefined;
	for (let x = raceRecord.raceTime - 1; x > 0; x--) {
		if (isBetter(x)) {
			bestMaxValue = x;
			break;
		}
	}

	if ((bestMinValue === undefined) || (bestMaxValue === undefined)) return undefined;
	else return (bestMaxValue - bestMinValue) + 1;
}

async function parseBoatRaceRecord(boatRaceRecordUrl, joinValues = false) {
	const boatRaceRecordFileDescriptor = await open(boatRaceRecordUrl);
	const recordValues = [];
	for await (const record of boatRaceRecordFileDescriptor.readLines()) {
		const [_description, ...values] = record.split(/\s+/);
		if (joinValues) {
			const joinedValues = values.reduce((prevValue, value) => prevValue + value, "");
			recordValues.push([joinedValues]);
		} else {
			recordValues.push(values);
		}
	}

	const [timeValues, distanceValues] = recordValues;
	if (timeValues.length !== distanceValues.length) throw new Error();

	return Array.from(zip(timeValues, distanceValues)).map(([timeValue, distanceValue]) => {
		return new RaceRecord(Number.parseInt(timeValue), Number.parseInt(distanceValue));
	});
}

class RaceRecord {

	constructor(raceTime, bestDistance) {
		this.raceTime = Number(raceTime);
		this.bestDistance = Number(bestDistance);
	}
}

// --- Day 5: If You Give A Seed A Fertilizer ---

import { open } from "node:fs/promises";
import { resolve } from "node:path";
import { collectFirst, fixedWindows, minOf, windowMap } from "./util/iterators.js";
import { groupBy } from "./util/maps.js";

export async function readAlmanac(almanacUrl = resolve("resources/day05/input01.txt")) {
	return await parseAlmanac(almanacUrl);
}

export function part1(almanac) {
	return minOf(almanac.collectSeedLocations(almanac.seeds));
}

export function part2(almanac) {
	function* collectSeedLocations(seedRanges) {
		for (const seedRange of seedRanges) {
			yield almanac.findMinSeedLocationWithin(seedRange);
		}
	}

	function* createSeedRanges(seedRangeDescriptors) {
		for (const [seedRangeStart, seedRangeLength] of seedRangeDescriptors) {
			yield new Range(seedRangeStart, seedRangeStart + seedRangeLength);
		}
	}

	const seedRangeDescriptors = fixedWindows(almanac.seeds, 2);
	const seedRanges = createSeedRanges(seedRangeDescriptors);
	return minOf(collectSeedLocations(seedRanges));
}

async function parseAlmanac(almanacUrl) {
	const rangePattern = /(?<rdest>\d+)\s+(?<rsrc>\d+)\s+(?<rlength>\d+)/;
	const categoryDescriptionPattern = /(?<source>[a-z]+)-to-(?<destination>[a-z]+).+/;

	async function readAllLines() {
		const lines = [];
		const almanacFileDescriptor = await open(almanacUrl);
		for await (const line of almanacFileDescriptor.readLines()) {
			lines.push(line);
		}

		return lines;
	}

	function parseRangeDescriptor(range) {
		const rangeMatch = range.match(rangePattern);
		if (!rangeMatch) {
			throw new Error(`Unable to parse range: ${range}.`);
		}

		const [_range, destRangeStart, srcRangeStart, rangeLength] = rangeMatch;
		return new RangeDescriptor(
			Number.parseInt(destRangeStart),
			Number.parseInt(srcRangeStart),
			Number.parseInt(rangeLength)
		);
	}

	function parseCategoryMap(categoryDescription, ranges) {
		const categoryDescriptionMatch = categoryDescription.match(categoryDescriptionPattern);
		if (!categoryDescriptionMatch) {
			throw new Error(`Unable to parse category map: ${categoryDescription}`);
		}

		const [_description, sourceCategory, destinationCategory] = categoryDescriptionMatch;
		const rangeDescriptors = ranges.map((range) => parseRangeDescriptor(range));
		return new CategoryMap(sourceCategory, destinationCategory, rangeDescriptors);
	}

	const almanacLines = await readAllLines();
	const [[seedsLine], ...groupedCategoryMaps] =
		windowMap(almanacLines,
			function openWhen(line) {
				return { openWindow: line.length, item: line };
			},
			function closeWhen(line) {
				return !line.length;
			},
			function transformLine(line) {
				return line;
			});

	const [_seedDescription, ...rawSeeds] = seedsLine.split(/\s+/);
	const seeds = rawSeeds.map((rawSeed) => Number.parseInt(rawSeed));
	const categoryMaps = groupedCategoryMaps.map(([categoryDescription, ...ranges]) => {
		return parseCategoryMap(categoryDescription, ranges);
	});

	return new Almanac(seeds, categoryMaps);
}

class Almanac {

	#categoryMapsBySrcCategory = new Map();

	constructor(seeds, categoryMaps) {
		this.seeds = seeds;
		this.categoryMaps = categoryMaps;

		this.#categoryMapsBySrcCategory = groupBy(categoryMaps, ({ sourceCategory }) => {
			return sourceCategory;
		});
	}

	findMinSeedLocationWithin(seedRange) {
		const seedCategoryMap = this.#categoryMapsBySrcCategory.get("seed");
		return this.#findMinCategoryNumberWithin("location", seedCategoryMap, [seedRange]);
	}

	*collectSeedLocations(seeds) {
		const seedCategoryMap = this.#categoryMapsBySrcCategory.get("seed");
		for (const seed of seeds) {
			yield this.#findCategoryNumber("location", seedCategoryMap, seed);
		}
	}

	#findMinCategoryNumberWithin(targetCategory, srcCategoryMap, srcRanges) {
		const splittedSrcRanges = srcCategoryMap.srcRanges().reduce((prevSrcRanges, categorySrcRange) => {
			return prevSrcRanges.flatMap((prevSrcRange) => prevSrcRange.split(categorySrcRange));
		}, srcRanges);

		const destRanges = splittedSrcRanges.map((srcRange) => srcCategoryMap.destRange(srcRange));
		if (srcCategoryMap.destinationCategory === targetCategory) {
			return minOf(destRanges.map(({ start }) => start));
		} else {
			const nextSrcCategoryMap = this.#categoryMapsBySrcCategory.get(srcCategoryMap.destinationCategory);
			return this.#findMinCategoryNumberWithin(targetCategory, nextSrcCategoryMap, destRanges);
		}
	}

	#findCategoryNumber(targetCategory, srcCategoryMap, src) {
		const dest = srcCategoryMap.destAt(src);
		if (srcCategoryMap.destinationCategory === targetCategory) return dest;
		else {
			const nextSrcCategoryMap = this.#categoryMapsBySrcCategory.get(srcCategoryMap.destinationCategory);
			return this.#findCategoryNumber(targetCategory, nextSrcCategoryMap, dest);
		}
	}
}

class CategoryMap {

	constructor(sourceCategory, destinationCategory, rangeDescriptors) {
		this.sourceCategory = sourceCategory;
		this.destinationCategory = destinationCategory;
		this.rangeDescriptors = rangeDescriptors;
	}

	srcRanges() {
		return this.rangeDescriptors.map(({ srcRange }) => srcRange);
	}

	destRange(srcRange) {
		const destStart = this.destAt(srcRange.start);
		const destEnd = this.destAt(srcRange.end - 1);
		return new Range(destStart, destEnd + 1);
	}

	destAt(src) {
		return collectFirst(this.rangeDescriptors, (rangeDescriptor) => {
			return rangeDescriptor.destAt(src);
		}) ?? src;
	}
}

class RangeDescriptor {

	constructor(destRangeStart, srcRangeStart, rangeLength) {
		this.destRange = new Range(destRangeStart, destRangeStart + rangeLength);
		this.srcRange = new Range(srcRangeStart, srcRangeStart + rangeLength);
	}

	destAt(src) {
		if (!this.srcRange.includes(src)) return undefined;
		else {
			const srcOffset = src - this.srcRange.start;
			const dest = this.destRange.start + srcOffset;
			if (this.destRange.includes(dest)) return dest;
			else return undefined;
		}
	}
}

class Range {

	constructor(start, end) {
		if (end <= start) throw new Error("end must be greater then start.");

		this.start = Number(start);
		this.end = Number(end);
	}

	static equal(leftRange, rightRange) {
		return (leftRange.start === rightRange.start) && (leftRange.end === rightRange.end);
	}

	includes(value) {
		return (value >= this.start) && (value < this.end);
	}

	intersects(otherRange) {
		return !(((otherRange.end <= this.start) || (otherRange.start >= this.end))
			|| ((this.start >= otherRange.end) || (this.end <= otherRange.start)));
	}

	split(range) {
		if (!this.intersects(range) || Range.equal(this, range)) return [this];
		else if (((range.start <= this.start) && (range.end >= this.end))) return [this];
		else if ((this.start < range.start) && (this.end > range.end)) {
			return [
				new Range(this.start, range.start),
				range,
				new Range(range.end, this.end)
			];
		} else if ((range.start <= this.start) && (range.end < this.end)) {
			return [
				new Range(this.start, range.end),
				new Range(range.end, this.end)
			];
		} else if ((range.start > this.start) && (range.end >= this.end)) {
			return [
				new Range(this.start, range.start),
				new Range(range.start, this.end)
			];
		} else {
			throw new Error("Unhandled input.");
		}
	}

	*[Symbol.iterator]() {
		for (let value = this.start; value < this.end; value++) yield value;
	}
}

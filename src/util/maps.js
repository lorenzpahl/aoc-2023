export function computeMapEntry(map, key, entryMapper) {
	if (key === undefined) throw new Error("undefined not allowed as key.");

	const currentValue = map.get(key);
	const newValue = entryMapper(key, currentValue);
	if (newValue !== undefined) return map.set(key, newValue);
	else {
		map.delete(key);
		return map;
	}
}

export function groupBy(sourceItems, classifier, itemTransformer = identity, remappingFunction = duplicateKeyErrorHandler) {
	const itemsByClassifier = new Map();
	for (const sourceItem of sourceItems) {
		const key = classifier(sourceItem);
		const value = itemTransformer(sourceItem);
		const currentValue = itemsByClassifier.get(key);
		if (currentValue !== undefined) {
			const remappedValue = remappingFunction(currentValue, value);
			itemsByClassifier.set(key, remappedValue);
		} else {
			itemsByClassifier.set(key, value);
		}
	}

	return itemsByClassifier;
}

export function groupIntoArrayBy(sourceItems, classifier, itemTransformer = identity) {
	const itemsByClassifier = new Map();
	for (const sourceItem of sourceItems) {
		const key = classifier(sourceItem);
		const item = itemTransformer(sourceItem);
		const items = itemsByClassifier.get(key);
		if (items) {
			items.push(item);
		} else {
			itemsByClassifier.set(key, [item]);
		}
	}

	return itemsByClassifier;
}

export function groupIntoSetBy(sourceItems, classifier, itemTransformer = identity) {
	const itemsByClassifier = new Map();
	for (let sourceItem of sourceItems) {
		const key = classifier(sourceItem);
		const item = itemTransformer(sourceItem);
		const items = itemsByClassifier.get(key);
		if (items) {
			items.add(item);
		} else {
			itemsByClassifier.set(key, new Set([item]));
		}
	}

	return itemsByClassifier;
}

function identity(item) {
	return item;
}

function duplicateKeyErrorHandler(currentValue, newValue) {
	throw new Error(`Unable to insert ${newValue}. Key is already associated with ${currentValue}.`);
}

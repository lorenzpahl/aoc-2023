export function collectFirst(soureItems, itemSelector) {
	for (const sourceItem of soureItems) {
		const item = itemSelector(sourceItem);
		if (item !== undefined) return item;
	}

	return undefined;
}

export function count(sourceItems, itemPredicate) {
	let count = 0;
	for (const sourceItem of sourceItems) {
		if (itemPredicate(sourceItem)) {
			count++;
		}
	}

	return count;
}

export function* distinctBy(sourceItems, keySelector) {
	const seenItems = new Set();
	for (const sourceItem of sourceItems) {
		const key = keySelector(sourceItem);
		if (!seenItems.has(key)) {
			yield sourceItem;
			seenItems.add(key);
		}
	}
}

export function fixedWindows(soureItems, windowSize) {
	if (windowSize <= 0) throw new Error(`the window size must be greater than 0.`);

	return windowMap(zipWithIndex(soureItems),
		function openWhen([sourceItem, _idx]) {
			return { openWindow: true, item: sourceItem };
		},
		function closeWhen([_sourceItem, idx]) {
			return idx % windowSize === 0;
		},
		function transformItem([sourceItem, _idx]) {
			return sourceItem;
		});
}

export function maxBy(sourceItems, itemComparator) {
	let maxItem = undefined;
	for (const sourceItem of sourceItems) {
		const cmp = itemComparator((maxItem ?? sourceItem), sourceItem);
		if ((maxItem === undefined) || (cmp > 0)) {
			maxItem = sourceItem;
		}
	}

	return maxItem;
}

export function minOf(numbers) {
	let minNumber = undefined;
	for (const number of numbers) {
		minNumber = Math.min(minNumber ?? number, number);
	}

	return minNumber;
}

export function productOf(numbers) {
	let product = 1;
	for (const number of numbers) {
		product *= number;
	}

	return product;
}

export function* repeatForever(sourceItemsSupplier) {
	while (true) yield* sourceItemsSupplier();
}

export function sumOf(numbers) {
	let sum = 0;
	for (const number of numbers) {
		sum += number;
	}

	return sum;
}

export function* slidingWindow(sourceItems, windowSize) {
	if (windowSize <= 0) throw new Error(`the window size must be greater than 0.`);

	let bucket = [];
	for (const sourceItem of sourceItems) {
		bucket.push(sourceItem);
		if (bucket.length === windowSize) {
			const lastItem = bucket.at(-1);

			yield bucket;
			bucket = [lastItem];
		}
	}
}

export function* windowMap(sourceItems, openWhen, closeWhen, itemTransformer) {
	let bucket = [];
	let windowOpen = false;
	let previousItem = undefined;
	for (let sourceItem of sourceItems) {
		const closeWindow = windowOpen ? closeWhen(sourceItem, previousItem) : false;
		const { openWindow, item } =
			(windowOpen && !closeWindow)
				? { openWindow: true, item: itemTransformer(sourceItem) }
				: openWhen(sourceItem);

		if (bucket.length && closeWindow && openWindow) {
			yield bucket;
			bucket = [item];
			windowOpen = true;
			previousItem = item;
		} else if (bucket.length && closeWindow) {
			yield bucket;
			bucket = [];
			windowOpen = false;
			previousItem = undefined;
		} else if (openWindow && !closeWindow) {
			bucket.push(item);
			windowOpen = true;
			previousItem = item;
		}
	}

	if (bucket.length) {
		yield bucket;
	}
}

export function windowUntil(sourceItems, closeWhen, itemTransformer = identity) {
	const openWhen = (sourceItem) => ({ openWindow: true, item: itemTransformer(sourceItem) });
	return windowMap(sourceItems, openWhen, closeWhen, itemTransformer);
}

export function* zip(leftSourceItems, rightSourceItems) {
	const leftSourceIterator = leftSourceItems[Symbol.iterator]();
	const rightSourceIterator = rightSourceItems[Symbol.iterator]();

	let leftResult = leftSourceIterator.next();
	let rightResult = rightSourceIterator.next();
	while (!leftResult.done && !rightResult.done) {
		yield [leftResult.value, rightResult.value];
		leftResult = leftSourceIterator.next();
		rightResult = rightSourceIterator.next();
	}
}

export function* zipWithIndex(soureItems) {
	let index = 0;
	for (const soureItem of soureItems) {
		yield [soureItem, index++];
	}
}

function identity(item) {
	return item;
}

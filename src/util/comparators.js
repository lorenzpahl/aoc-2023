export function comparing(keyExtractor, comparator) {
	return (lhs, rhs) => {
		const lhsKey = keyExtractor(lhs);
		const rhsKey = keyExtractor(rhs);
		return comparator(lhsKey, rhsKey);
	};
}

export function comparingNumber(keyExtractor) {
	return (lhs, rhs) => {
		const a = keyExtractor(lhs);
		const b = keyExtractor(rhs);
		return (a < b) ? -1 : ((a > b) ? +1 : 0);
	};
}

export function reversedComparator(originalComparator) {
	return (lhs, rhs) => {
		const cmp = originalComparator(lhs, rhs);
		return (cmp < 0) ? +1 : ((cmp > 0) ? -1 : cmp);
	};
}

export function thenComparing(firstComparator, secondComparator) {
	return (lhs, rhs) => {
		const cmp = firstComparator(lhs, rhs);
		return (cmp !== 0) ? cmp : secondComparator(lhs, rhs);
	};
}

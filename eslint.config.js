import globals from "globals";
import js from "@eslint/js";
import stylistic from "@stylistic/eslint-plugin";

export default [
	js.configs.recommended,
	{
		files: ["**/*.js"],
		languageOptions: {
			ecmaVersion: "latest",
			sourceType: "module",
			globals: {
				...globals.browser
			}
		},
		plugins: {
			"@stylistic": stylistic
		},
		rules: {
			"@stylistic/arrow-parens": [
				"warn",
				"always"
			],
			"@stylistic/semi": [
				"error",
				"always"
			],
			"@stylistic/quotes": [
				"error",
				"double",
				{
					"avoidEscape": true,
					"allowTemplateLiterals": true
				}
			],
			"no-unused-vars": [
				"warn",
				{
					"vars": "all",
					"args": "after-used",
					"argsIgnorePattern": "^_",
					"destructuredArrayIgnorePattern": "^_"
				}
			]
		}
	}
];

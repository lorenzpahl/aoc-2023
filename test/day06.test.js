import assert from "node:assert";
import { resolve } from "node:path";
import { describe, it } from "node:test";
import { part1, part2 } from "../src/day06.js";

describe("day 6", () => {
	describe("examples", () => {
		it("[part 1] the product of the number of ways you can beat the record should be 288", async () => {
			const result = await part1(resolve("resources/day06/example01.txt"));
			assert.strictEqual(result, 288);
		});

		it("[part 2] you should be able to beat the record in 71503 ways", async () => {
			const result = await part2(resolve("resources/day06/example01.txt"));
			assert.strictEqual(result, 71_503);
		});
	});

	describe("solutions", () => {
		it("[part 1] the product of the number of ways you can beat the record should be 2269432", async () => {
			const result = await part1();
			assert.strictEqual(result, 2_269_432);
		});

		it("[part 2] you should be able to beat the record in 35865985 ways", async () => {
			const result = await part2();
			assert.strictEqual(result, 35_865_985);
		});
	});
});

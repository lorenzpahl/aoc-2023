import assert from "node:assert";
import { resolve } from "node:path";
import { describe, it } from "node:test";
import { createEngineSchematic, part1, part2 } from "../src/day03.js";

describe("day 3", () => {
	describe("examples", () => {
		it("[part 1] the sum of all of the part numbers should be 4361", async () => {
			const engineSchematic = await createEngineSchematic(resolve("resources/day03/example01.txt"));
			const sumOfPartNumbers = part1(engineSchematic);
			assert.strictEqual(sumOfPartNumbers, 4_361);
		});

		it("[part 2] the sum of all of the gear ratios should be 467835", async () => {
			const engineSchematic = await createEngineSchematic(resolve("resources/day03/example01.txt"));
			const sumOfGearRatios = part2(engineSchematic);
			assert.strictEqual(sumOfGearRatios, 467_835);
		});
	});

	describe("solutions", () => {
		it("[part 1] the sum of all of the part numbers should be 539637", async () => {
			const engineSchematic = await createEngineSchematic();
			const sumOfPartNumbers = part1(engineSchematic);
			assert.strictEqual(sumOfPartNumbers, 539_637);
		});

		it("[part 2] the sum of all of the gear ratios should be 82818007", async () => {
			const engineSchematic = await createEngineSchematic();
			const sumOfGearRatios = part2(engineSchematic);
			assert.strictEqual(sumOfGearRatios, 82_818_007);
		});
	});
});

import assert from "node:assert";
import { resolve } from "node:path";
import { describe, it } from "node:test";
import { part1, part2, readAlmanac } from "../src/day05.js";

describe("day 5", () => {
	describe("examples", () => {
		it("[part 1] the lowest location number should be 35", async () => {
			const almanac = await readAlmanac(resolve("resources/day05/example01.txt"));
			const lowestLocationNumber = part1(almanac);
			assert.strictEqual(lowestLocationNumber, 35);
		});

		it("[part 2] the lowest location number should be 46", async () => {
			const almanac = await readAlmanac(resolve("resources/day05/example01.txt"));
			const lowestLocationNumber = part2(almanac);
			assert.strictEqual(lowestLocationNumber, 46);
		});
	});

	describe("solutions", () => {
		it("[part 1] the lowest location number should be 389056265", async () => {
			const almanac = await readAlmanac();
			const lowestLocationNumber = part1(almanac);
			assert.strictEqual(lowestLocationNumber, 389_056_265);
		});

		it("[part 2] the lowest location number should be 137516820", async () => {
			const almanac = await readAlmanac();
			const lowestLocationNumber = await part2(almanac);
			assert.strictEqual(lowestLocationNumber, 137_516_820);
		});
	});
});

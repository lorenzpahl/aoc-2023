import assert from "node:assert";
import { resolve } from "node:path";
import { describe, it } from "node:test";
import { part1, part2 } from "../src/day02.js";

describe("day 2", () => {
	describe("examples", () => {
		it("[part 1] the sum of the IDs should be 8", async () => {
			const sumOfIds = await part1(undefined, resolve("resources/day02/example01.txt"));
			assert.strictEqual(sumOfIds, 8);
		});

		it("[part 2] the sum of the power should be 2286", async () => {
			const sumOfPower = await part2(resolve("resources/day02/example01.txt"));
			assert.strictEqual(sumOfPower, 2_286);
		});
	});

	describe("solutions", () => {
		it("[part 1] the sum of the IDs should be 2085", async () => {
			const sumOfIds = await part1();
			assert.strictEqual(sumOfIds, 2_085);
		});

		it("[part 2] the sum of the power should be 79315", async () => {
			const sumOfPower = await part2();
			assert.strictEqual(sumOfPower, 79_315);
		});
	});
});

import assert from "node:assert";
import { resolve } from "node:path";
import { describe, it } from "node:test";
import { part1, part2 } from "../src/day09.js";

describe("day 9", () => {
	describe("examples", () => {
		it("[part 1] the sum of the extrapolated values should be 114", async () => {
			const sumOfExtrapolatedValues = await part1(resolve("resources/day09/example01.txt"));
			assert.strictEqual(sumOfExtrapolatedValues, 114);
		});

		it("[part 2] the sum of the extrapolated previous values should be 2", async () => {
			const sumOfExtrapolatedValues = await part2(resolve("resources/day09/example01.txt"));
			assert.strictEqual(sumOfExtrapolatedValues, 2);
		});
	});

	describe("solutions", () => {
		it("[part 1] the sum of the extrapolated values should be 1637452029", async () => {
			const sumOfExtrapolatedValues = await part1();
			assert.strictEqual(sumOfExtrapolatedValues, 1_637_452_029);
		});

		it("[part 2] the sum of the extrapolated previous values should be 908", async () => {
			const sumOfExtrapolatedValues = await part2();
			assert.strictEqual(sumOfExtrapolatedValues, 908);
		});
	});
});

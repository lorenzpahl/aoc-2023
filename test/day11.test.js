import assert from "node:assert";
import { resolve } from "node:path";
import { describe, it } from "node:test";
import { part1, part2 } from "../src/day11.js";

describe("day 11", () => {
	describe("examples", () => {
		it("[part 1] the sum of the lengths should be 374", async () => {
			const sumOfLengths = await part1(resolve("resources/day11/example01.txt"));
			assert.strictEqual(sumOfLengths, 374);
		});

		it("[part 2.1] the sum of the lengths should be 1030", async () => {
			const sumOfLengths = await part2(resolve("resources/day11/example01.txt"), 10);
			assert.strictEqual(sumOfLengths, 1_030);
		});

		it("[part 2.2] the sum of the lengths should be 8410", async () => {
			const sumOfLengths = await part2(resolve("resources/day11/example01.txt"), 100);
			assert.strictEqual(sumOfLengths, 8_410);
		});
	});

	describe("solutions", () => {
		it("[part 1] the sum of the lengths should be 10422930", async () => {
			const sumOfLengths = await part1();
			assert.strictEqual(sumOfLengths, 10_422_930);
		});

		it("[part 2] the sum of the lengths should be 699909023130", async () => {
			const sumOfLengths = await part2();
			assert.strictEqual(sumOfLengths, 699_909_023_130);
		});
	});
});

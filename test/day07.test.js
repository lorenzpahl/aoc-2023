import assert from "node:assert";
import { resolve } from "node:path";
import { describe, it } from "node:test";
import { part1, part2 } from "../src/day07.js";

describe("day 7", () => {
	describe("examples", () => {
		it("[part 1] the total winnings should be 6440", async () => {
			const totalWinnings = await part1(resolve("resources/day07/example01.txt"));
			assert.strictEqual(totalWinnings, 6_440);
		});

		it("[part 2] using the new joker rule, the total winnings should be 5905", async () => {
			const totalWinnings = await part2(resolve("resources/day07/example01.txt"));
			assert.strictEqual(totalWinnings, 5_905);
		});
	});

	describe("solutions", () => {
		it("[part 1] the total winnings should be 250232501", async () => {
			const totalWinnings = await part1();
			assert.strictEqual(totalWinnings, 250_232_501);
		});

		it("[part 2] using the new joker rule, the total winnings should be 249138943", async () => {
			const totalWinnings = await part2();
			assert.strictEqual(totalWinnings, 249_138_943);
		});
	});
});

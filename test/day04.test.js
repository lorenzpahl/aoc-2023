import assert from "node:assert";
import { resolve } from "node:path";
import { describe, it } from "node:test";
import { part1, part2, readAllScratchcards } from "../src/day04.js";

describe("day 4", () => {
	describe("examples", () => {
		it("[part 1] the cards should be worth 13 points in total", async () => {
			const scratchcards = await readAllScratchcards(resolve("resources/day04/example01.txt"));
			const sumOfScratchcardPoints = part1(scratchcards);
			assert.strictEqual(sumOfScratchcardPoints, 13);
		});

		it("[part 2] you should have a total of 30 scratchcards", async () => {
			const scratchcards = await readAllScratchcards(resolve("resources/day04/example01.txt"));
			const numberOfScratchcards = part2(scratchcards);
			assert.strictEqual(numberOfScratchcards, 30);
		});
	});

	describe("solutions", () => {
		it("[part 1] the cards should be worth 23028 points in total", async () => {
			const scratchcards = await readAllScratchcards();
			const sumOfScratchcardPoints = part1(scratchcards);
			assert.strictEqual(sumOfScratchcardPoints, 23_028);
		});

		it("[part 2] you should have a total of 9236992 scratchcards", async () => {
			const scratchcards = await readAllScratchcards();
			const numberOfScratchcards = part2(scratchcards);
			assert.strictEqual(numberOfScratchcards, 9_236_992);
		});
	});
});

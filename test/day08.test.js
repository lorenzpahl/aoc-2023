import assert from "node:assert";
import { resolve } from "node:path";
import { describe, it } from "node:test";
import { part1, part2 } from "../src/day08.js";

describe("day 8", () => {
	describe("examples", () => {
		it("[part 1.1] it should take 2 steps to reach ZZZ", async () => {
			const stepCount = await part1(resolve("resources/day08/example01.txt"));
			assert.strictEqual(stepCount, 2);
		});

		it("[part 1.2] it should take 6 steps to reach ZZZ", async () => {
			const stepCount = await part1(resolve("resources/day08/example02.txt"));
			assert.strictEqual(stepCount, 6);
		});

		it("[part 2] it should take 6 steps until all nodes end with Z", async () => {
			const stepCount = await part2(resolve("resources/day08/example03.txt"));
			assert.strictEqual(stepCount, 6);
		});
	});

	describe("solutions", () => {
		it("[part 1] it should take 19637 steps to reach ZZZ", async () => {
			const stepCount = await part1();
			assert.strictEqual(stepCount, 19_637);
		});

		it("[part 2] it should take 8811050362409 steps until all nodes end with Z", async () => {
			const stepCount = await part2();
			assert.strictEqual(stepCount, 8_811_050_362_409);
		});
	});
});

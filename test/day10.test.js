import assert from "node:assert";
import { resolve } from "node:path";
import { describe, it } from "node:test";
import { part1, part2 } from "../src/day10.js";

describe("day 10", () => {
	describe("examples", () => {
		it("[part 1.1] it should take 4 steps to get to the point farthest from the starting position", async () => {
			const stepCount = await part1(resolve("resources/day10/example01.txt"));
			assert.strictEqual(stepCount, 4);
		});

		it("[part 1.2] it should take 8 steps to get to the point farthest from the starting position", async () => {
			const stepCount = await part1(resolve("resources/day10/example02.txt"));
			assert.strictEqual(stepCount, 8);
		});

		it("[part 2.1] 4 tiles should be enclosed by the loop", async () => {
			const enclosedTileCount = await part2(resolve("resources/day10/example03.txt"));
			assert.strictEqual(enclosedTileCount, 4);
		});

		it("[part 2.2] 8 tiles should be enclosed by the loop", async () => {
			const enclosedTileCount = await part2(resolve("resources/day10/example04.txt"));
			assert.strictEqual(enclosedTileCount, 8);
		});

		it("[part 2.3] 10 tiles should be enclosed by the loop", async () => {
			const enclosedTileCount = await part2(resolve("resources/day10/example05.txt"));
			assert.strictEqual(enclosedTileCount, 10);
		});
	});

	describe("solutions", () => {
		it("[part 1] it should take 7173 steps to get to the point farthest from the starting position", async () => {
			const stepCount = await part1();
			assert.strictEqual(stepCount, 7_173);
		});

		it("[part 2] 291 tiles should be enclosed by the loop", async () => {
			const enclosedTileCount = await part2();
			assert.strictEqual(enclosedTileCount, 291);
		});
	});
});

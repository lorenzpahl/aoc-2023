import assert from "node:assert";
import { resolve } from "node:path";
import { describe, it } from "node:test";
import { part1, part2 } from "../src/day01.js";

describe("day 1", () => {
	describe("examples", () => {
		it("[part 1] should produce 142", async () => {
			const calibrationValueSum = await part1(resolve("resources/day01/example01.txt"));
			assert.strictEqual(calibrationValueSum, 142);
		});

		it("[part 2] should produce 281", async () => {
			const calibrationValueSum = await part2(resolve("resources/day01/example02.txt"));
			assert.strictEqual(calibrationValueSum, 281);
		});
	});

	describe("solutions", () => {
		it("[part 1] should produce 55477", async () => {
			const calibrationValueSum = await part1();
			assert.strictEqual(calibrationValueSum, 55_477);
		});

		it("[part 2] should produce 54431", async () => {
			const calibrationValueSum = await part2();
			assert.strictEqual(calibrationValueSum, 54_431);
		});
	});
});

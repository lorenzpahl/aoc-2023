# Advent of Code 2023 - Solutions

https://adventofcode.com/2023

## Puzzles

| Day  | Done  | Puzzle |
| :--- | :---: | :----: |
| 1  | ✔ | [Trebuchet?!](https://adventofcode.com/2023/day/1) \[[solution](src/day01.js)\]                                 |
| 2  | ✔ | [Cube Conundrum](https://adventofcode.com/2023/day/2) \[[solution](src/day02.js)\]                              |
| 3  | ✔ | [Gear Ratios](https://adventofcode.com/2023/day/3) \[[solution](src/day03.js)\]                                 |
| 4  | ✔ | [Scratchcards](https://adventofcode.com/2023/day/4) \[[solution](src/day04.js)\]                                |
| 5  | ✔ | [If You Give A Seed A Fertilizer](https://adventofcode.com/2023/day/5) \[[solution](src/day05.js)\]             |
| 6  | ✔ | [Wait For It](https://adventofcode.com/2023/day/6) \[[solution](src/day06.js)\]                                 |
| 7  | ✔ | [Camel Cards](https://adventofcode.com/2023/day/7) \[[solution](src/day07.js)\]                                 |
| 8  | ✔ | [Haunted Wasteland](https://adventofcode.com/2023/day/8) \[[solution](src/day08.js)\]                           |
| 9  | ✔ | [Mirage Maintenance](https://adventofcode.com/2023/day/9) \[[solution](src/day09.js)\]                          |
| 10 | ✔ | [Pipe Maze](https://adventofcode.com/2023/day/10) \[[solution](src/day10.js)\]                                  |
| 11 | ✔ | [Cosmic Expansion](https://adventofcode.com/2023/day/11) \[[solution](src/day11.js)\]                           |
| 12 | ✘ |                                                                                                                 |
| 13 | ✘ |                                                                                                                 |
| 14 | ✘ |                                                                                                                 |
| 15 | ✘ |                                                                                                                 |
| 16 | ✘ |                                                                                                                 |
| 17 | ✘ |                                                                                                                 |
| 18 | ✘ |                                                                                                                 |
| 19 | ✘ |                                                                                                                 |
| 20 | ✘ |                                                                                                                 |
| 21 | ✘ |                                                                                                                 |
| 22 | ✘ |                                                                                                                 |
| 23 | ✘ |                                                                                                                 |
| 24 | ✘ |                                                                                                                 |
| 25 | ✘ |                                                                                                                 |

## Build and Run

Clone the repository:

```bash
$ git clone https://gitlab.com/lorenzpahl/aoc-2023.git
$ cd ./aoc-2023
```

Build and test:

```bash
$ npm install
$ npm test
```

Run one of the solutions:

```bash
$ npm start
```
